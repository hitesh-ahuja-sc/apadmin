import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LiveTrackingComponent } from './live-tacking.component';
// import { PrimeNgInputComponent } from './input/primeng-input.component';
// import { PrimeNgButtonComponent } from './button/primeng-button.component';
// import { PrimeNgPanelComponent } from './panel/primeng-panel.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DefaultComponent } from '../default.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { SharedModule } from '../../../../shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';


import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { NgSelectModule } from '@ng-select/ng-select';


const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: LiveTrackingComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                    // { path: 'button', component: PrimeNgButtonComponent },
                    // { path: 'panel', component: PrimeNgPanelComponent },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        ToastModule.forRoot(),
        // primeng modules
        NgSelectModule,
        NgxSpinnerModule
    ],
    declarations: [
        LiveTrackingComponent
        // PrimeNgInputComponent,
        // PrimeNgButtonComponent,
        // PrimeNgPanelComponent
    ]
})
export class LiveTrackingModule {
}
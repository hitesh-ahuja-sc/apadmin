export class DeviceModel {
    deviceType: string;
    imei: string;
    userNumber: string;
    vehicle_number: string;
    chassis_number: string;
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DeviceComponent } from './device.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DefaultComponent } from '../default.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { SharedModule } from '../../../../shared/shared.module';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerModule } from 'ngx-spinner';

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: DeviceComponent,
                children: [

                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgSelectModule,
        ToastModule.forRoot(),
        NgxSpinnerModule,
    ],
    declarations: [
        DeviceComponent,

    ]
})
export class DeviceModule {
}
import {
    Component,
    OnInit,
    ViewEncapsulation,
    ViewContainerRef,
    ViewChild,
    ElementRef
} from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MapService } from "../../../../shared/services/map.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MapJSON } from "./mapjson";
import { NgxSpinnerService } from 'ngx-spinner';

declare var google: any;
import * as $ from "jquery";
import * as _ from "lodash";
import { DeviceService } from "../../../../shared/services/device.service";
import * as moment from "moment";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { interval } from "rxjs/observable/interval";
declare var GMaps: any;
declare var MapLabel: any;
@Component({ selector: "app-map", templateUrl: "./map.component.html", styleUrls: ["./map.component.scss"] })
export class MapComponent implements OnInit {
    @ViewChild("map") map: ElementRef;
    vehicleForm: FormGroup;
    devices: any = [];
    mapMovementInterval: any = [];
    mapData;
    movementCount = 0;
    mapJSON = new MapJSON();
    path: any = [];
    totalPoints = 0;
    infoMessages = [];
    mapDetails: any = [];
    showFlag: boolean;
    marker;
    mapResponseData: any = [];
    currentLocation: {
        lat: Number;
        long: Number
    } = {
        lat: 19.2403,
        long: 73.1305
    };
    mapObj: {
        map: any;
        mapOptions: any;
        lineSymbol: object;
        line: any,
        markerList: any,
        infoWindowList: any
    } = {
        map: {},
        mapOptions: {},
        lineSymbol: {},
        line: [],
        markerList: [],
        infoWindowList: []
    };
    imei = [];

    timeObj: any = {
        start: moment()
            .startOf("day")
            .toDate(),
        end: moment()
            .endOf("day")
            .toDate()
    };
    startdate: Date = new Date();
    enddate: Date = new Date();

    settings = {
        bigBanner: true,
        timePicker: true,
        format: "dd-MMM-yyyy hh:mm",
        defaultOpen: false
    };
    mapStatus = {
        start: false,
        pause: false,
        stop: false
    };
    filterData: any = {
        imei: "",
        result: []
    };
    speedArray = [{ label: '1X', value: 1 }, { label: '2X', value: 2 }, { label: '3X', value: 3 }, { label: '4X', value: 4 }, { label: '5X', value: 5 }, { label: '6X', value: 6 }, { label: '7X', value: 7 }, { label: '8X', value: 8 }, { label: '9X', value: 9 }, { label: '10X', value: 10 }];
    selectedSpeed = 1;
    constructor(private spinnerService: NgxSpinnerService, private mapService: MapService, private deviceService: DeviceService, private fb: FormBuilder, public toastr: ToastsManager, vcr: ViewContainerRef, ) {
        this
            .toastr
            .setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.spinnerService.hide();
        this.initVehicleForm();
        this.getDeviceDetails();
        this.getCorrentPosition();
        this.initMap();
    }
    ngOnDestroy() {
        // clearInterval( this.interval);
        console.log("destroy", this.mapMovementInterval);
        this
            .mapMovementInterval
            .forEach(interval => {
                clearInterval(interval)
            });
    }
    /*initialize vehcile form*/
    initVehicleForm() {
        this.vehicleForm = this.fb.group({ imei: [] });
    }

    /*get current position of client*/
    getCorrentPosition() {
        let self = this;
        navigator.geolocation.getCurrentPosition(position => {
            console.log("position : ", position);
            self.currentLocation.lat = Number(position.coords.latitude);
            self.currentLocation.long = Number(position.coords.longitude);
            localStorage.setItem("currentLocation", JSON.stringify({ lat: self.currentLocation.lat, long: self.currentLocation.long }));
        });
    }

    /* select vehicles*/
    selectVehicle(imei) {
        // clearInterval(this.mapMovementInterval);
        console.log("imei : ", imei);
        this.imei = imei.imei;
        // this.mapStatus.start = true;
        // this.mapStatus.pause = false;
        this.resetMap();
        // setTimeout(()=>{ },1500)
    }

    /*get location by using imei*/
    getDeviceDetails() {
        this
            .deviceService
            .getAllDevice()
            .subscribe((data: any) => {
                this.devices = data.data;
                console.log("deviceService is", this.devices);
            });
    }
    selectSpeedMethod() {
        console.log('this.selectedSpeed', this.selectedSpeed);
        // this.mapStatus.start = true;
        this.resetMap();
    }

    /*initialize map with default co-ordinates using current lat long*/
    initMap() {
        let location = localStorage.getItem("currentLocation");
        if (location != undefined) {
            this.currentLocation = JSON.parse(location);
            console.log("currentLocation : ", this.currentLocation);
        }
        let self = this;
        this.mapObj.mapOptions = {
            center: {
                lat: this.currentLocation.lat,
                lng: this.currentLocation.long
            },
            zoom: 13,
            mapTypeId: "terrain"
        };
        this.mapObj.map = new google
            .maps
            .Map(this.map.nativeElement, this.mapObj.mapOptions);

        this.findVehicleAndSetIcon();

    }
    findVehicleAndSetIcon() {

        if (this.imei && this.imei.length > 0) {

            this
                .imei
                .forEach((imei) => {
                    // console.log('imei : ',imei);
                    const matchedObj = _.find(this.devices, { imei: imei })
                    // console.log('matchedObj : ',matchedObj);
                    if (matchedObj.vehicle && matchedObj.vehicle.binid) {
                        // console.log('matchedObj.vehcile : ', matchedObj.vehicle)
                        const color = this.getRandomeColor();
                        localStorage.setItem('color', color)
                        matchedObj
                            .vehicle
                            .binid
                            .forEach((binObj) => {
                                // console.log('binObj : ',binObj) const coordsObj=Object.assign({},binObj);
                                this.checkkGeofenceType(binObj)
                            })
                    }
                })
        }

    }

    checkkGeofenceType(bin) {
        // let pos = null;



        console.log('bin ; ', bin);
        switch (bin.loc.type.toLowerCase()) {
            case 'polygon':
                {
                    // console.log('Bin typpe is polygon : ', bin.loc.coordinates)
                    const coordinates = bin.loc.coordinates[0];
                    const location = { lat: coordinates[0][0], lng: coordinates[0][1] }
                    // console.log('Ploygon latLng : ',location);
                    const latLngArray = this.setLatLongInObj(bin.loc.coordinates[0]);
                    // myOptions.position = latLng;
                    // new google.maps.drawOverlay({
                    //     lat: location.lat,
                    //     lng: location.lng,
                    //     content: '<div class="overlay">Lima</div>'
                    //   });
                    //   pos = new google.maps.LatLng(location.lat, location.lng);
                    // mapLabel.set('position', new google.maps.LatLng(location.lat, location.lng));
                    // console.log('latLngArray : ', latLngArray);
                    this.setPolygon(latLngArray, bin)
                    break;
                }
            case 'point':
                {
                    // console.log('Bin typpe is point : ')
                    // console.log('Default : ', bin.loc.coordinates)
                    const location = { lat: bin.loc.coordinates[0], lng: bin.loc.coordinates[1] };
                    // console.log('Circle latLng : ',location);
                    // pos = new google.maps.LatLng(location.lat, location.lng);
                    this.setCircle(location, bin)
                    break;
                }
            default:
                {
                    console.log('Default : ', bin.loc.coordinates)
                    // const latLng={lat:bin.loc.coordinates[0].lat,lng:bin.loc.coordinates[0].lng}
                    // this.setCircle(latLng)
                    break;
                }
        }
        // let marker = new google.maps.Marker({
        //     position: pos,
        //     map: this.mapObj.map,
        //     title: 'Title',
        //     icon: 'https://www.google.com/support/enterprise/static/geo/cdate/art/dots/red_dot.png'

        // });

        // /// LABEL ///
        // var label = new Label({
        //     map: this.mapObj.map,
        //     text: bin.geo_nm
        // });
        // label.bindTo('position', marker, 'position');

    }

    setPolygon(latLngArray, bin) {

        const color = localStorage.getItem('color') != undefined
            ? localStorage.getItem('color')
            : this.getRandomeColor();
        // console.log('color :', color)
        var polyGonTriangle = new google
            .maps
            .Polygon({
                paths: latLngArray, strokeColor: color, //'#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: color, //this.getRandomeColor(),
                fillOpacity: 0.2
            });
        polyGonTriangle.setMap(this.mapObj.map);
        // }, 1000)
        var infoWindow = new google.maps.InfoWindow;
        console.log("polyGonTriangle", polyGonTriangle, bin.geo_nm)

        polyGonTriangle.addListener('click', function(event) {
            // Since this polygon has only one path, we can call getPath() to return the
            // MVCArray of LatLngs.
            var self: any = this;
            var vertices = self.getPath();
            var infoWindow = new google.maps.InfoWindow;
            var contentString = "<b>" + bin.geo_nm + "</b>";
            console.log("at showArrays", event);


            // Replace the info window's content and position.
            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);

            infoWindow.open(this.map);
        });

    }

    setCircle(latLng, bin) {
        // console.log('latLng : ', latLng);
        const color = localStorage.getItem('color') != undefined
            ? localStorage.getItem('color')
            : this.getRandomeColor();
        var cityCircle = new google.maps.Circle({
            strokeColor: color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.2,
            map: this.mapObj.map,
            center: latLng,
            radius: 120
        });
        var infoWindow = new google.maps.InfoWindow;
        console.log("polyGoncircle", cityCircle)

        cityCircle.addListener('click', function(event) {
            // Since this polygon has only one path, we can call getPath() to return the
            // MVCArray of LatLngs.
            var self: any = this;
            // var vertices = self.getPath();
            var infoWindow = new google.maps.InfoWindow;
            var contentString = "<b>" + bin.geo_nm + "</b>";
            console.log("at showArrays", event);


            // Replace the info window's content and position.
            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);

            infoWindow.open(this.map);
        });
    }

    getRandomeColor() {
        var hex = (Math.round(Math.random() * 0xffffff)).toString(16);
        while (hex.length < 6) hex = "0" + hex;
        return hex;
    }
    setLatLongInObj(array) {
        // console.log('setLatLongInObj array : ',array)
        let newArray = []
        array.forEach((coords, i) => {
            newArray.push({ lat: coords[0], lng: coords[1] })
            //   console.log('triangleCoords : ',array[i])
        })
        return newArray;
    }

    start() {
        // console.log('this.totalPoints ', this.totalPoints)
        // console.log('self.movementCount : ', this.movementCount)

        console.log('this.mapObj : ', this.mapObj);
        if (this.mapResponseData.length > 0) {
            console.log('If : ', this.mapObj);
            const validCount = _.find(this.mapResponseData, (element) => {
                return element.movementCount > 0;
            })
            console.log('validCount : ', validCount);
            if (validCount && validCount.movementCount > 0) {
                this.mapResponseData.forEach((element, index) => {
                    this.animateCircle(element, this.mapObj.line[index], index);
                })
                this.mapStatus.start = false;
                this.mapStatus.pause = true;
                this.mapStatus.stop = true;
            }
            else {
                this.mapResponseData = [];
            }
        }

        if (this.mapResponseData.length == 0) {
            this.spinnerService.show();
            /* when puase button is clicked*/
            let obj: any = {
                imei: this.imei,
                startTime: moment(this.timeObj.start).format(),
                endTime: moment(this.timeObj.end).format()
            }
            // console.log('obj : ',obj);
            var opt = { minZoom: 13, maxZoom: 19 };
            this.mapObj.map.setOptions(opt);
            this.mapResponseData = [];
            this.resetMap();
            this.mapService.getDeviceRouteReplay(obj).subscribe((response: any) => {
                console.log("success data", response);
                this.spinnerService.hide();
                this.mapResponseData = response.data;
                console.log("this.mapResponseData : ", this.mapResponseData)
                this.drawmap();
            }, (error: any) => {
                this.spinnerService.hide();
                this
                    .toastr
                    .error("No Data found", "!Error");
            });
            this.showFlag = false;
            this.mapStatus.start = false;
            this.mapStatus.pause = true;
            console.log("start routing : ");
        }


    }

    drawmap() {
        let index = 0;

        this.mapResponseData.forEach(element => {
            console.log('element.result.length : ', element)
            if (element.result.length > 0) {
                let coordinates = element.result[0];
                element.path = [];
                this.filterLocations(element, coordinates)
                // setTimeout(()=>{
                console.log('element : ', element.path)
                if (element.path.length > 0) {
                    this.updateMap(element, element.path, index);
                    index = index + 1;

                }
            } else {
                this.toastr.error("No Data found", "!Error");

            }

            // },5000)

        });
    }

    /*filter locations*/
    filterLocations(trackData, coordiates) {

        _.forEach(trackData.result, (ele) => {
            if (ele.speed > 0) {
                ele.lng = ele.long
                trackData.path.push(ele)
            }
        });

        if (trackData.path.length > 0) {
            console.log('path at filterLocation():', trackData.path.length)
            return trackData;
        } else {
            console.log("mapdata at else", trackData)
            let position = {
                lat: coordiates.lat,
                lng: coordiates.long
            };
            setTimeout(() => {
                var marker = new google.maps.Marker({ position: position, map: this.mapObj.map, title: 'not moving' });
                var label = new google.maps.InfoWindow({ content: "not moving" });
                label.open(this.mapObj.map, marker)
                // this.updateMap(trackData.result)
            }, 1000)
            return null;
        }

    }

    /* select/change date */
    onDateSelect(event: any) {
        event = moment(event).utc().format();
        console.log("event", event);
        this.timeObj = {
            start: moment(event).format(),
            end: moment(event).endOf("day").toDate()
        };

        this.resetMap();
        console.log("timeObj", this.timeObj);
    }

    setStartDate(event) {
        console.log('startDate : ', event, this.startdate);
        // if(diff>0)
        // {
        const start = moment(moment(this.startdate).format('YYYY-MM-DD'));
        const today = moment(moment(this.enddate).format('YYYY-MM-DD'));
        const end = moment(moment(this.enddate).format('YYYY-MM-DD'));
        const diff = Number(today.diff(start, 'days'));
        console.log('diff : ', diff);
        if (diff >= 0) {
            const diff2 = Number(end.diff(start, 'days'));
            console.log('diff2: ', diff2);
            if (diff2 <= 0) {
                this.enddate = new Date(this.startdate);
            }
        }
        else {
            this.enddate = new Date(this.startdate);
        }
        this.setEndDate(this.enddate);
        // }
        // this.setQueryString();
    }
    setEndDate(event) {
        console.log('endDate : ', this.enddate);
        const start = moment(moment(this.startdate).format('YYYY-MM-DD'));
        const end = moment(moment(this.enddate).format('YYYY-MM-DD'));
        const diff = Number(end.diff(start, 'days'));
        console.log('end.diff(start,"days") : ', end.diff(start, 'days'));
        if (diff == 0) {
            this.timeObj = {
                start: this.startdate,
                end: this.enddate
            };
            console.log("this.timeObj", this.timeObj);
            this.resetMap();
            // this.setQueryString();
        } else {
            // this.toastr.error('Select Same date for routing');
            this.enddate = new Date(this.startdate);
            // this.cdr.detectChanges();
        }
        return diff;
        // console.log('')
    }
    updateMap(trackObj, path, index) {
        console.log(path.length, 'path')
        // trackData = { imei: 12345, result: [] }
        var lastIndex,
            start,
            end;
        const self = this;
        lastIndex = path.length - 1;
        start = path[0];
        end = path[lastIndex]
        // this.showFlag = true; this.filterLocations();
        this.mapObj.map.setZoom(13); // This will trigger a zoom_changed on the map
        if (path !== undefined && path.length > 0) {
            // console.log("In IF case", this.path)
            console.log('path[0] : ', path[0])
            this.mapObj.map.setCenter(new google.maps.LatLng(path[0].lat, path[0].long));
            this.mapObj.lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                scale: 4,
                strokeColor: "#393"
            };
            const line = new google.maps.Polyline({
                path: [],
                map: this.mapObj.map,
                strokeColor: "#4a6ec6"
            });
            this.mapObj.line.push(line);
            // var icon = {
            //     url: "https://banner2.kisspng.com/20180302/luq/kisspng-waste-garbage-truck-icon-garbage-truck-png-vector-material-5a999e11c95b93.5142181615200169138248.jpg", // url
            //     scaledSize: new google.maps.Size(50, 50), // size
            // };
            var myLatLng = new google.maps.LatLng(path[0].lat, path[0].long);
            const marker = new google.maps.Marker({
                position: myLatLng,
                map: this.mapObj.map//,
                // icon:icon
            });
            var infowindow = new google.maps.InfoWindow({
                content: trackObj.vehicleNumber + ' time : ' + moment().format('LT')
            });
            console.log('infowindow : ', infowindow);
            infowindow.open(self.mapObj.map, marker);
            marker.addListener('click', function() {
                console.log('marker clicked: ', marker);
                infowindow.open(self.mapObj.map, marker);
            });
            this.mapObj.infoWindowList.push(infowindow);;
            this.mapObj.markerList.push(marker);
            //const marker = marker.setPosition(new google.maps.LatLng(0,0));
            trackObj.movementCount = 0;
            this.animateCircle(trackObj, this.mapObj.line[index], index);
        }
    }

    setMarkers(trackObj, start, end) {
        console.log('devices');
        const self = this;
        var marker1 = new google
            .maps
            .Marker({
                position: {
                    lat: start.lat,
                    lng: start.long
                },
                map: this.mapObj.map,
                title: 'start'
            });
        var marker2 = new google
            .maps
            .Marker({
                position: {
                    lat: end.lat,
                    lng: end.long
                },
                map: this.mapObj.map,
                title: 'end'
            });

        // var startLabel = new google
        //     .maps
        //     .InfoWindow({content: trackObj.vehicleNumber});
        // var endLabel = new google
        //     .maps
        //     .InfoWindow({content: trackObj.vehicleNumber});
        // startLabel.open(this.mapObj.map, marker1)
        // endLabel.open(this.mapObj.map, marker2)

        var infowindow1 = new google.maps.InfoWindow({
            content: trackObj.vehicleNumber
        });

        var infowindow2 = new google.maps.InfoWindow({
            content: trackObj.vehicleNumber
        });
        infowindow1.open();
        infowindow2.open();
        marker1.addListener('click', function() {
            console.log('marker clicked: ', marker1);
            infowindow1.open(self.mapObj.map, marker1);
        });

        marker2.addListener('click', function() {
            console.log('marker clicked: ', marker2);
            infowindow2.open(self.mapObj.map, marker2);
        });
    }

    animateCircle(trackObj, line, index) {
        console.log("AnimateCircle called : ", trackObj, line);
        let self = this;
        var path = line.getPath();
        console.log('path : ', path);

        if (self.mapMovementInterval[index] == undefined) {
            self.mapMovementInterval.push("");

        }
        // var count = 0; clearInterval(self.mapMovementInterval);
        this.mapMovementInterval[index] = window.setInterval(function() {

            var icons = line.get("icons");
            // let step_perc = Math.round(Number(trackObj.movementCount * 100) / trackObj.result.length);
            // console.log('trackObj.movementCount : ', trackObj.movementCount);
            let trackingData = trackObj.result[trackObj.movementCount];


            /*map will clear here */
            // console.log('trackingData : ',trackingData);
            if (trackingData == undefined) {

                self.spinnerService.hide();
                console.log("clear map")
                // this.mapData = []; this.totalPoints = 0;
                trackObj.movementCount = 0;
                console.log('checkMovingStatus -> self.mapMovementInterval[index] : ', self.mapMovementInterval[index]);
                self.checkMovingStatus();

                clearInterval(self.mapMovementInterval[index]);
            }
            // console.log('trackingData : ', trackObj.movementCount);

            else if (trackingData != undefined) {
                const latLng = new google.maps.LatLng(trackingData.lat, trackingData.long)
                // console.log('latLng : ',latLng);
                console.log('trackingData moving : ', trackingData.speed);
                //
                trackObj.movementCount = Number(trackObj.movementCount) + 1;
                const status = trackingData.speed == 0 ? " (Not moving)" : "";
                // self.mapObj.markerList[index].setPosition(new google.maps.LatLng(trackingData.lat, trackingData.long));

                // self.mapObj.map.panTo(new google.maps.LatLng(lat, long));
                // console.log('self.mapObj.infoWindowList[index] : ',self.mapObj.infoWindowList[index])
                if (trackingData.speed > 0) {
                    // console.log('step_perc : ', step_perc, self.selectedSpeed);
                    console.log('IN IF - trackObj : ', trackObj)
                    self.spinnerService.hide();
                    let lat = trackingData.lat,
                        long = trackingData.long;
                    path.push(latLng);
                    trackObj.lastSpeed = trackingData.speed;
                    /* To move map along with marker */
                    // self.mapObj.map.panTo(new google.maps.LatLng(lat, long));
                    var movingStatus = null;
                    self.mapObj.markerList[index].setPosition(new google.maps.LatLng(trackingData.lat, trackingData.long));
                    self.mapObj.infoWindowList[index].setContent("speed : " + trackingData.speed + status + ", time : " + moment(trackingData.time).format('LTS'));
                    self.mapObj.infoWindowList[index].open(self.mapObj.map, self.mapObj.markerList[index]);
                }
                else {
                    console.log('IN ELSE - trackObj : ', trackObj)
                    if (trackObj.lastSpeed == undefined || trackObj.lastSpeed > 0) {
                        self.mapObj.markerList[index].setPosition(new google.maps.LatLng(trackingData.lat, trackingData.long));
                        self.mapObj.infoWindowList[index].setContent("speed : " + trackingData.speed + status + ", time : "
                            + moment(trackingData.time).format('LTS'));
                        self.mapObj.infoWindowList[index].open(self.mapObj.map, self.mapObj.markerList[index]);
                    }
                    trackObj.lastSpeed = trackingData.speed;
                }
                // icons[0].offset = trackObj.movementCount * 100 / trackObj.result.length + "%";
                // line.set("icons", icons);
            } else {
                console.log('FINISHED -> trackingData : ', trackingData)
                clearInterval(self.mapMovementInterval[index]);
            }

            // }, (1000 / this.selectedSpeed));
        }, (1));
    }

    checkMovingStatus() {
        console.log('checkMovingStatus : ', this.mapResponseData);
        // this.mapResponseData.forEach((device)=>{
        //     console.log('device : ', device, device.movementCount);
        // })
        const matchedObj = _.find(this.mapResponseData, (device) => {
            return device.movementCount > 0;
        });
        if (matchedObj == undefined) {
            this.mapStatus.start = true;
            this.mapStatus.pause = false;
            this.mapStatus.stop = false;
        }
        console.log('checkMovingStatus matchedObj : ', matchedObj)
    }

    /*pause vehicle map*/
    pause() {
        // // this.mapStatus.pause = false; this.mapStatus = {     start: true, pause:
        // false }; console.log("pause routing : ");
        // clearInterval(this.mapMovementInterval);
        this.mapStatus.start = true;
        this.mapStatus.pause = false;
        this.mapStatus.stop = true;
        // this.resetMap();
        this.clearAllInterval()
    }

    stop() {
        console.log('stop called')
        this.mapStatus.start = true;
        this.mapStatus.pause = false;
        this.mapStatus.stop = false;
        this.resetMap();
    }

    clearAllInterval() {
        this.mapMovementInterval.forEach((interval, index) => {
            console.log('interval, index : ', interval, index)
            clearInterval(interval);
        });
    }
    /*reset vehicle map*/
    resetMap() {
        // // this.resetMap
        console.log("resetMap() : ");
        this.mapStatus.start = true;
        this.mapStatus.pause = false;
        this.mapStatus.stop = false;
        // this.mapData = []; this.totalPoints = 0; console.log(this.mapObj.line);
        let self = this;
        this.mapMovementInterval.forEach((interval, index) => {
            console.log('interval, index : ', interval, index)
            clearInterval(interval);
        });
        self.mapObj.line.forEach((lineObj) => {
            lineObj.setMap(null);
        });
        self.mapObj.markerList.forEach((marker) => {
            marker.setMap(null);
        })
        self.mapObj.line = [];
        self.mapObj.markerList = [];
        this.movementCount = 0;
        this.mapResponseData = [];
        // this.marker.setMapOnAll(null);
        this.initMap();

    }
}

import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
// import { Observable } from 'rxjs/Rx';
import "rxjs/Rx";

// import 'rxjs/add/operator/map';
import { ReportsModel } from "../../theme/pages/default/reports/reports.component.model";
import { Observable } from "rxjs/Observable";
@Injectable()
export class ReportsService {
    constructor(private http: Http) { }

    /**
     * Get all Vehicle Details.
     * @return {Observable<Response>} response with user Data.
     */
    getByQuery(query): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get single Vehicle details.
     * @param id Vehicle id.
     * @return {Observable<Response>} current User data.
     */
    getReportById(report) {
        // console.log("report at service", report)
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/location/vehicle?imei=" + report.imei + "&startTime=" + report.start_date + "&endTime=" + report.end_date, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    /**
     * Get single Vehicle details.
     * @param id Vehicle id.
     * @return {Observable<Response>} current User data.
     */
    getReportByBin(report) {
        // console.log("report at service", report)
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http.get("/location/geo/single?geo=" + report.bin + "&&startTime=" + report.start_date + "&&endTime=" + report.end_date, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
      * Error Handler.
      * @param {any} err error if any
      * @return {err} throw error if any
      */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
// import { Observable } from 'rxjs/Rx';
import "rxjs/Rx";

// import 'rxjs/add/operator/map';
// import { UserModel } from "../../theme/pages/default/user/user.component.model";
import { Observable } from "rxjs/Observable";
@Injectable()
export class UserService {
    constructor(private http: Http) { }

    /**
     * Get all User Details.
     * @return {Observable<Response>} response with user Data.
     */
    getAllUser(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    block(id): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        return this.http
            .put("https://api.police2.fortunekit.com/api/user/block/" + id, {})
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    unblock(id): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        return this.http
            .put("https://api.police2.fortunekit.com/api/user/unblock/" + id, {})
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    getAllPoliceUser(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/user/", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    /**
     * Get all User Details.
     * @return {Observable<Response>} response with user Data.
     */
    getByQuery(query): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get User by pagination.
     * @return {Observable<Response>} response with user Data.
     */
    getByPagination(query, action, id): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query + "&" + action + "=" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Save user data.
     * @param {UserModel} user user object
     * @return {Observable<Response>} response with user data.
     */
    // user: UserModel
    createUser(user: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const option = new RequestOptions({ headers: headers });
        return this.http
            .post("/user", user, option)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get single user details.
     * @param id user id.
     * @return {Observable<Response>} current User data.
     */
    getUserById(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Delete single user details.
     * @param id user id.
     * @return {Observable<Response>} deleted user data.
     */
    deleteUser(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .delete("/user/" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Update single user details.
     * @param user user updated Data.
     * @param id string user id.
     * @return {Observable<Response>} current status data.
     */
    updateUser(id: any, user: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .put("/user/" + id, user, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    /**
    * Get global search result.
    * @return {Observable<Response>} response with global search Data.
    */
    getGlobalSearch(search): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user/search?search=" + search, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Error Handler.
     * @param {any} err error if any
     * @return {err} throw error if any
     */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

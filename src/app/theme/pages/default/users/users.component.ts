import {
    Component,
    OnInit,
    ViewEncapsulation,
    ViewContainerRef
} from "@angular/core";
import { UserService } from "../../../../shared/services/user.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { FileUploadService } from "../../../../shared/services/file-upload.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: "app-users",
    templateUrl: "./users.component.html",
    styleUrls: ["./users.component.scss"]
})
export class UsersComponent implements OnInit {
    users: any = [];
    // adminData: any = [];
    addUserForm: FormGroup;
    editForm: FormGroup;
    selectedUser: any = {};
    filteredItems: any = [];
    items: any = [];
    inputName: string = "";
    previous: string;
    next: string;
    index;
    userDetails: any = [];
    constructor(
        private spinnerService: NgxSpinnerService,
        private userService: UserService,
        private fb: FormBuilder,
        public toastr: ToastsManager,
        vcr: ViewContainerRef,
        private fileUploadService: FileUploadService
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }
    ngOnInit() {
        this.spinnerService.show();
        this.getUserDetails();
        this.addUserForm = this.fb.group({
            user_name: [null, Validators.compose([Validators.required, Validators.maxLength(24)])],
            user_mobile: [
                null,
                Validators.compose([Validators.required, Validators.maxLength(10)])
            ],
            user_password: [null, Validators.compose([Validators.required, Validators.maxLength(8)])],
            // user_role: [null, Validators.required],
            user_email: [null, Validators.required],
        });

        this.editForm = this.fb.group({

            name: [null, Validators.compose([Validators.required, Validators.maxLength(24)])],
            mobile: [null, Validators.compose([Validators.required])],
            email: [null, Validators.required],
            // organization: [null, Validators.required],
            // address: [null, Validators.required],
            // pan: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
            // gst: [null, Validators.required]
        });

    }

    getUserDetails() {
        this.items = []
        this.userService.getAllUser().subscribe((data: any) => {
            this.userDetails = data.data;
            this.spinnerService.hide();
            // this.previous = data.result.previous;
            // this.next = data.result.next;
            // this.items = [];
            // this.adminDetails = [];
            console.log("User details:", this.userDetails)
            this.items = this.userDetails;
            console.log("User data is :", this.items);
        }, (error) => {
            this.spinnerService.hide();
        });
    }

    FilterByName() {
        this.filteredItems = [];
        console.log("input name", this.inputName);
        console.log(this.userDetails);
        if (this.inputName != "") {
            this.userDetails.forEach(element => {
                if (
                    element.name.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    0 ||
                    element.mobile.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    0 ||
                    element.email
                        .toUpperCase()
                        .indexOf(this.inputName.toUpperCase()) >= 0
                ) {
                    this.filteredItems.push(element);
                }
            });
        } else {
            this.filteredItems = this.userDetails;
        }
        console.log(this.filteredItems);
        this.items = this.filteredItems
    }

    previousPage() {
        console.log("previous", this.previous);
        this.items = [];
        this.userService.getByPagination("admin", "previous", this.previous).subscribe((data: any) => {
            if (data.result.previous !== null) {
                this.userDetails = data.result.data;
                console.log('items after previous', this.userDetails)
                this.items = this.userDetails;
                this.previous = data.result.previous;
                this.next = data.result.next;
                console.log("previous:" + this.previous + " next: " + this.next)
            }
            else {
                this.userDetails = data.result.data;
                this.items = this.userDetails;
                this.next = data.result.next;
                this.previous = null;
                console.log("previous:" + this.previous + " next: " + this.next)
            }
        });
    }

    nextPage() {
        console.log("next", this.next);
        this.items = [];
        this.userDetails = [];
        this.userService.getByPagination("admin", "next", this.next).subscribe((data: any) => {
            if (data.result.next !== null) {
                this.userDetails = data.result.data;
                console.log('items after next', this.userDetails);
                this.items = this.userDetails;
                this.previous = data.result.previous;
                this.next = data.result.next;
                console.log("previous:" + this.previous + " next: " + this.next)
            } else {
                this.userDetails = data.result.data;
                this.items = this.userDetails;
                this.previous = data.result.previous;
                this.next = null
                console.log("previous:" + this.previous + " next: " + this.next)
            }
        });
    }

    addUser(user: any) {
        this.spinnerService.show();
        let newUser: any = {
            name: user.user_name,
            mobile: user.user_mobile,
            password: user.user_password,
            role: "driver",
            email: user.user_email
        };
        this.userService.createUser(newUser).subscribe(
            data => {
                setTimeout(() => {
                    console.log("new user data:==>", data);
                    // this.adminData = [];
                    // this.getUserDetails();
                    this.items.push(data.data);
                    this.spinnerService.hide();
                    this.toastr.success("User Added Succesfully", "!Added");
                }, 1000);
            },
            (error: any) => {
                this.spinnerService.hide();
                const err = JSON.parse(error);
                console.log("Error:", err.message);
                this.toastr.error(err.message, "Error");
                // this.adminData = [];
                this.getUserDetails();
            }
        );
        console.log("Created User is:", newUser);
    }

    openModal(user: any, index) {
        this.index = index;

        // console.log('index is', index)
        if (user) {
            this.selectedUser = user;
            console.log(user);
            this.editForm = this.fb.group({
                name: [this.selectedUser.name, Validators.required],
                mobile: [this.selectedUser.mobile, Validators.compose([Validators.required, Validators.maxLength[10]])],
                email: [this.selectedUser.email, Validators.required]
            });
        }
    }

    editUserDetails(modalData: any) {

        modalData.mobile = modalData.mobile.toString();
        let updateUser = {};
        console.log("ModalData will be:", modalData);
        // console.log("selectedAdmin id:", this.selectedAdmin._id);
        setTimeout(() => {
            updateUser = {
                name: modalData.name,
                mobile: modalData.mobile,
                // role: "driver",
                email: modalData.email,
            };
            this.userService
                .updateUser(this.selectedUser._id, updateUser)
                .subscribe(
                data => {
                    this.toastr.success("User updated successfully", "Updated!");

                    // this.getUserDetails();
                    this.items.splice(this.index, 1, data.data);

                },
                (error: any) => {
                    const err = JSON.parse(error);
                    this.toastr.error(err.message, "Error");
                    this.getUserDetails();

                }
                );
        }, 1000);
    }

    viewUserProfile(user: any) {
        this.selectedUser = user;
        console.log(this.selectedUser);
    }

    deleteUser() {
        console.log('this.index', this.index);
        console.log(this.selectedUser);
        setTimeout(() => {
            this.userService.deleteUser(this.selectedUser._id).subscribe(data => {
                this.toastr.success("User deleted successfully", "Deleted!");

                // this.getUserDetails();
                this.items.splice(this.index, 1);

            });
        }, 1000);
    }

    clearField() {
        this.addUserForm.reset();
    }

}

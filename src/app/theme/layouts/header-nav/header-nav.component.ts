import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { UserService } from '../../../shared/services/user.service';
import { Router } from '@angular/router';
declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    styleUrls: ['./header-nav.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    user: any = [];
    inputSearch: string = "";
    searchData = [];
    users: any = [];
    device: any = [];
    organization: any = [];

    constructor(private userService: UserService, private router: Router) {

    }
    ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.user = currentUser;
    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }

    searchByName() {
        console.log('inputName', this.inputSearch);

        this.userService.getGlobalSearch(this.inputSearch).subscribe((data: any) => {
            this.searchData = data.result;
            this.device = data.result.device;
            this.users = data.result.user;
            this.organization = data.result.organization

            // var length = Object.keys(this.searchData).length;
            console.log("searchData", this.searchData);
        }, (error: any) => {
            let err = JSON.parse(error);
            console.log("statusCode", err.statusCode);
            if (err.statusCode == 404) {
                this.device = [];
                this.users = [];
                this.organization = [];

            }
        })

    }
    goToRouteReplay(device) {
        // let id: string = "1234567890"
        console.log("device obj from header:", device.vehicleNumber)
        localStorage.setItem('deviceForRoute', device);
        this.router.navigate(['/device/device-summary', device.vehicleNumber]);
    }

    logout() {
        localStorage.clear();
    }

}
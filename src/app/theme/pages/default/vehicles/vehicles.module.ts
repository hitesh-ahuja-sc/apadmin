import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesComponent } from './vehicles.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DefaultComponent } from '../default.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { SharedModule } from '../../../../shared/shared.module';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { AssignBinComponent } from './assign-bin/assign-bin.component';
import { NgSelectModule } from '@ng-select/ng-select';
const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: VehiclesComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                ]
            }, {
                path: "assign-bin",
                component: AssignBinComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgSelectModule,
        ToastModule.forRoot(),
    ],
    declarations: [
        VehiclesComponent,
        AssignBinComponent,
        // PrimeNgInputComponent,
        // PrimeNgButtonComponent,
        // PrimeNgPanelComponent
    ]
})
export class VehiclesModule {
}
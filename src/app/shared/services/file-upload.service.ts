import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
// import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

// import { Observable } from 'rxjs/Observable';
@Injectable()
export class FileUploadService {

    constructor(private http: Http) { }


    uploadFile(fileToUpload: File) {

        let formData: FormData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        // console.log("formData", formData);
        const headers = new Headers({});
        headers.append('Authorization', 'jwt ' + localStorage.getItem('token'));

        const option = new RequestOptions({ headers: headers });
        // console.log('option', headers)
        return this.http.post('/upload/5ae05d8b8604cd73bd7fb46e', formData, option).map((res: Response) => res.json()).catch(this.handleError);
    }

    uploadUserExcel(file) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'jwt ' + localStorage.getItem('token'));

        const option = new RequestOptions({ headers: headers });
        // console.log('option', headers)
        return this.http.post('/user/userThroughExcel', file, option).map((res: Response) => res.json()).catch(this.handleError);
    }
    uploadCompanyExcel(file) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'jwt ' + localStorage.getItem('token'));

        const option = new RequestOptions({ headers: headers });
        // console.log('option', headers)
        return this.http.post('/company/createThroughExcel', file, option).map((res: Response) => res.json()).catch(this.handleError);
    }

    uploadOrderExcel(file) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'jwt ' + localStorage.getItem('token'));

        const option = new RequestOptions({ headers: headers });
        // console.log('option', headers)
        return this.http.post('/order/createThroughExcel', file, option).map((res: Response) => res.json()).catch(this.handleError);
    }

    handleError(err: any) {
        return Observable.throw(err);
    }

}



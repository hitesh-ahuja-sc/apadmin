import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '../../../../shared/shared.module';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { TrackingUserComponent } from './tacking-user.component';




const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: TrackingUserComponent,
                children: []
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        ToastModule.forRoot(),
        NgSelectModule,
        NgxSpinnerModule
    ],
    declarations: [
        TrackingUserComponent
    ]
})
export class TrackingUserModule {
}
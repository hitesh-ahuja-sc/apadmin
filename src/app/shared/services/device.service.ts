import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
// import { Observable } from 'rxjs/Rx';
import "rxjs/Rx";

// import 'rxjs/add/operator/map';
import { DeviceModel } from "../../theme/pages/default/device/device.component.model";
import { Observable } from "rxjs/Observable";
@Injectable()
export class DeviceService {
    constructor(private http: Http) { }

    /**
     * Get all Device Details.
     * @return {Observable<Response>} response with user Data.
     */
    getAllDevice(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/device", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    getAllCases() {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/request/cases/all", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    getCaseDetails(caseId) {
        return this.http
            .get("https://api.police2.fortunekit.com/api/request/cases/" + caseId)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    /**
     * Get all Bins List.
     * @return {Observable<Response>} response with user Data.
     */
    getAllBinsList(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/geo", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    /**
     * Get all Device Details.
     * @return {Observable<Response>} response with user Data.
     */
    getByQuery(query): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get Vehicle by pagination.
     * @return {Observable<Response>} response with user Data.
     */
    getByPagination(query, action, id): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query + "&" + action + "=" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Save user data.
     * @param {VehicleModel} user user object
     * @return {Observable<Response>} response with user data.
     */

    createDevice(device: DeviceModel) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const option = new RequestOptions({ headers: headers });
        return this.http
            .post("/device", device, option)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get single device details.
     * @param id device id.
     * @return {Observable<Response>} current User data.
     */
    getDeviceById(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/device" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Delete single device details.
     * @param id device id.
     * @return {Observable<Response>} deleted device data.
     */
    deleteDevice(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .delete("/device/" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Update single device details.
     * @param device updated Data.
     * @param id string device id.
     * @return {Observable<Response>} current status data.
     */
    updateDevice(id: any, device: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .put("/device/" + id, device, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    /**
    * Get global search result.
    * @return {Observable<Response>} response with global search Data.
    */
    getGlobalSearch(search): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/device/search?search=" + search, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Error Handler.
     * @param {any} err error if any
     * @return {err} throw error if any
     */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

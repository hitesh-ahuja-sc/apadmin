import {
    Component,
    OnInit,
    Input,
    EventEmitter,
    Output,
    ViewContainerRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { VehiclesService } from "../../../../../shared/services/vehicles.service";

declare var jQuery: any;

import { ToastsManager } from "ng2-toastr/ng2-toastr";


@Component({ selector: 'app-assign-bin', templateUrl: './assign-bin.component.html', styleUrls: ['./assign-bin.component.scss'] })
export class AssignBinComponent implements OnInit {
    assignBinForm: FormGroup;
    @Output() updateBins = new EventEmitter();
    @Input('refreshBins') refreshBins;
    bins = [];
    loading = false;
    constructor(private fb: FormBuilder, private vehicleService: VehiclesService, private toaster: ToastsManager, private vcr: ViewContainerRef) {
        this.toaster.setRootViewContainerRef(vcr);
    }

    ngOnInit() {

        this.assignBinForm = this
            .fb
            .group({
                bins: [null, Validators.required]
            });

    }

    ngOnChanges() {
        this.bins = this.refreshBins.bins;
        if (this.assignBinForm) {
            this
                .assignBinForm
                .reset();
            setTimeout(() => {
                console.log('selectecd bins : ', this.refreshBins.vehicle.binid)
                this
                    .assignBinForm
                    .patchValue({ bins: this.refreshBins.vehicle.binid })
            }, 100)
        }
        console.log('assigned Bins : ', this.refreshBins);
    }

    selectBins($event) {
        console.log('selected bin : ', $event)
    }
    assignBinToVehicle() {
        this.loading = true;
        console.log('formData : ', this.assignBinForm.value)
        const assignObj = {
            "imei": this.refreshBins.vehicle.imei,
            "vehicle_number": this.refreshBins.vehicle.vehicle_number,
            "chassis_number": this.refreshBins.vehicle.chassis_number,
            "binid": this.assignBinForm.value['bins']
        }
        console.log('assigned object : ', assignObj)
        // setTimeout(() => { }, 2000)
        this
            .vehicleService
            .assignBinToVehicle(this.refreshBins.vehicle._id, assignObj)
            .subscribe((response) => {
                console.log('resposne : ', response)
                this.loading = false;
                this.updateBins.emit(response.data);
                jQuery('#assignVehicleModal').modal('hide');
                this.toaster.success("Bins Assigned Succesfully", "Success");
            }, (error) => {
                console.log('Error : ', error);
                this.toaster.success(error.message, "!Error");
            })
    }
}

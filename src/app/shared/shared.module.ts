import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { UserService } from "../shared/services/user.service";
import { LocationService } from "../shared/services/location.service";
import { DeviceService } from "../shared/services/device.service";
import { CapitalizePipe } from "./pipes/capitalize.pipe";
import { CommonModule } from "@angular/common";
import { FileUploadService } from "./services/file-upload.service";
// import { CompanyService } from "./services/company.service";
// import { OrderService } from "./services/order.service";
import { RolesDirective } from "./directives/roles.directive";
import { MapService } from "./services/map.service";
// import { AlertService } from "./services/alert.service";
// import { SosService } from "./services/sos.service";
// import { PanicService } from "./services/panic.service";
// import { ChecklistService } from "./services/checklist.service";
import { FormatDateTimePipe } from "./pipes/formate-date-time.pipe";
import { VehiclesService } from "./services/vehicles.service";
import { ReportsService } from "./services/reports.service";
import { RequestService } from "./services/request.service";

@NgModule({
    declarations: [CapitalizePipe, FormatDateTimePipe, RolesDirective],
    imports: [CommonModule],
    exports: [CapitalizePipe, FormatDateTimePipe, RolesDirective],
    providers: [
        UserService,
        RequestService,
        LocationService,
        DeviceService,
        ReportsService,
        // Ord1erService,
        MapService,
        FileUploadService,
        // AlertService,
        // SosService,
        // PanicService,
        // Checkli  stService,
        VehiclesService
    ],
    bootstrap: []
})
export class SharedModule { }

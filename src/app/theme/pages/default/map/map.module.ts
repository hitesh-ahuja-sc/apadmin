import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MapComponent } from "./map.component";
import { ReactiveFormsModule } from "@angular/forms";
import { DefaultComponent } from "../default.component";
import { LayoutModule } from "../../../layouts/layout.module";
import { SharedModule } from "../../../../shared/shared.module";
import { AngularDateTimePickerModule } from "angular2-datetimepicker";
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastModule } from "ng2-toastr/ng2-toastr";
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: MapComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                    // { path: 'button', component: PrimeNgButtonComponent },
                    // { path: 'panel', component: PrimeNgPanelComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        ToastModule.forRoot(),
        // primeng modules
        AngularDateTimePickerModule,
        NgSelectModule,
        NgxSpinnerModule

    ],
    declarations: [
        MapComponent
        // PrimeNgInputComponent,
        // PrimeNgButtonComponent,
        // PrimeNgPanelComponent
    ]
})
export class MapModule { }

import { BaseRequestOptions, Http, RequestMethod, RequestOptions, Response, ResponseOptions, XHRBackend } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { request } from "http";
export function mockBackEndFactory(backend: MockBackend, options: BaseRequestOptions, realBackend: XHRBackend) {
    // array in local storage for registered users
    let users: any[] = JSON.parse(localStorage.getItem('users')) || [];
    // fake token
    let token: string = 'fake-jwt-token';

    // configure fake backend
    backend.connections.subscribe((connection: any) => {
        // wrap in timeout to simulate server api call
        // console.log("connection : ", connection)
        setTimeout(() => {
            // let serverUrl="http://13.232.112.43:3031/api"+ connection.request.url; // FCA
            let serverUrl;
            const tempUrl: String = connection.request.url;
            if (tempUrl.includes('http')) {
                serverUrl = connection.request.url; // Live
            } else {
                serverUrl = "https://api.police.fortunekit.com/api" + connection.request.url; // Live
            }
            // let serverUrl = "http://192.168.0.114:3031/api" + connection.request.url; // Local FCA Server
            // let serverUrl = "http://13.126.85.223:3031/api" + connection.request.url; // New TEST Server
            let body = connection.request.getBody();
            // console.log("connection.request.getBody() : ", body, typeof body)
            if (connection.request._body != null && typeof body != "object") {
                body = JSON.parse(body)
            }

            // pass through any requests not handled above
            let realHttp = new Http(realBackend, options);
            let requestOptions = new RequestOptions({
                method: connection.request.method,
                headers: connection.request.headers,
                body: connection.request._body ? body : null,
                url: serverUrl,
                withCredentials: connection.request.withCredentials,
                responseType: connection.request.responseType
            });
            realHttp.request(connection.request.url, requestOptions)
                .subscribe((response: Response) => {
                    connection.mockRespond(response);
                },
                (error: any) => {
                    console.log("error : ", error);
                    if (error.status == 401) {
                        localStorage.clear();
                        window.location.href = "/";
                    }
                    connection.mockError(error);
                });

        }, 500);

    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    deps: [MockBackend, BaseRequestOptions, XHRBackend],
    useFactory: mockBackEndFactory
};
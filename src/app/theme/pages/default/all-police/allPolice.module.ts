import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AllPoliceComponent } from './allPolice.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DefaultComponent } from '../default.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { SharedModule } from '../../../../shared/shared.module';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: AllPoliceComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        ToastModule.forRoot(),
        NgxSpinnerModule
    ],
    declarations: [
        AllPoliceComponent,
        // PrimeNgInputComponent,
        // PrimeNgButtonComponent,
        // PrimeNgPanelComponent
    ]
})
export class AllPoliceModule {
}
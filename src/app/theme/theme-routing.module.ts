import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/_guards/auth.guard';
// import { LoginModule } from './login/login.module';
// import { UserModule } from '../theme/user/user.module';

const routes: Routes = [
    {
        'path': '',
        'component': ThemeComponent,
        'canActivate': [AuthGuard],
        'children': [
            {
                "path": "index",
                // 'loadChildren': '.\/pages\/default\/dashboard\/dashboard.module#DashboardModule',
                "loadChildren": ".\/pages\/default\/users\/users.module#UsersModule",
            },
            // {
            //     "path": "user",
            //     "loadChildren": ".\/pages\/default\/user\/user.module#UserModule"
            // },
            {
                "path": "users",
                "loadChildren": ".\/pages\/default\/users\/users.module#UsersModule"
            },
            {
                "path": "allpolice",
                "loadChildren": ".\/pages\/default\/all-police\/allPolice.module#AllPoliceModule"
            },
            {
                "path": "allcase",
                "loadChildren": ".\/pages\/default\/all-case\/all-case.module#AllCaseModule"
            },
            {
                "path": "vehicles",
                "loadChildren": ".\/pages\/default\/vehicles\/vehicles.module#VehiclesModule"
            },
            {
                "path": "device",
                "loadChildren": ".\/pages\/default\/device\/device.module#DeviceModule"
            },
            // {
            //     "path": "reports",
            //     "loadChildren": ".\/pages\/default\/reports\/reports.module#ReportsModule"
            // },
            {
                'path': '',
                'redirectTo': 'live-tracking',
                'pathMatch': 'full',
            },
            {
                "path": "route-replay",
                "loadChildren": ".\/pages\/default\/map\/map.module#MapModule"
            },
            {
                "path": "tracking-user",
                "loadChildren": ".\/pages\/default\/tracking-user\/tracking-user.module#TrackingUserModule"
            },
            {
                "path": "live-tracking",
                "loadChildren": ".\/pages\/default\/live-tracking\/live-tracking.module#LiveTrackingModule"
            },
            {
                "path": "money-request",
                "loadChildren": ".\/pages\/default\/money-request\/money-request.module#MoneyRequestModule"
            },
            {
                "path": "travel-request",
                "loadChildren": ".\/pages\/default\/travel-request\/travel-request.module#TravelRequestModule"
            },
        ],
    },
    {
        'path': '**',
        'redirectTo': 'live-tracking',
        'pathMatch': 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ThemeRoutingModule { }
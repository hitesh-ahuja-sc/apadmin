import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
@Injectable()
export class RequestService {
    constructor(private http: Http) { }

    /**
     * Get all User Locations.
     * @return {Observable<Response>} response with user Data.
     */
    allPendingService(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        // headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        // const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/amountRequest/travelRequest/all")
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    
    allTravelRequestTransaction(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        // headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        // const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/amountRequest/travelRequest/all")
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    
    allTravelRequestPendingService(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        // headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        // const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/amountRequest/travelRequest/all")
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    allTransaction(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        // headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        // const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/amountRequest")
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    approveRequest(amount: Number, id: String): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        return this.http
            .put("https://api.police2.fortunekit.com/api/amountRequest/approve/" + id,
            {
                "amountApproved": amount.toString()
            })
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    rejectUserRequest(id: String) {

        return this.http
            .put("https://api.police2.fortunekit.com/api/amountRequest/reject/" + id,
            {})
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Error Handler.
     * @param {any} err error if any
     * @return {err} throw error if any
     */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

import { Component, OnInit, ViewEncapsulation, ViewContainerRef } from "@angular/core";
// import { DeviceService } from "../../../../shared/services/device.service";
import { Router } from "@angular/router";
import * as _ from "lodash";
import * as moment from "moment";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
// import { FileUploadService } from
// "../../../../shared/services/file-upload.service";
import { DeviceService } from "../../../../shared/services/device.service";
import { ReportsService } from "../../../../shared/services/reports.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ selector: "app-reports", templateUrl: "./reports.component.html", styleUrls: ["./reports.component.scss"] })
export class ReportsComponent implements OnInit {
    reportsArr: any = [];
    imei: string;
    selectedBin: string;
    startDate: string;

    endDate: string;
    reports: any = ['Vehicle Wise', 'Bin Wise'];
    devices: any = [];
    binsArray: any = [];
    reportDetails: any = [];
    reportsForm: FormGroup;
    public maxDate = new Date();
    dateFlag = true;
    processingHttp: Boolean = false;
    start_date: Date = new Date();
    // end_date : Date = new Date();
    settings = {
        bigBanner: false,
        timePicker: false,
        format: 'dd-MM-yyyy',
        defaultOpen: false,
        closeOnSelect: true,
        rangepicker: true
    };

    constructor(private reportsService: ReportsService, private deviceService: DeviceService, private spinnerService: NgxSpinnerService, private fb: FormBuilder, public toastr: ToastsManager, vcr: ViewContainerRef, ) {
        this
            .toastr
            .setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getDeviceDetails();
        this.getBinsList();
        this.reportsForm = this.fb.group({
            reportType: new FormControl(null, Validators.required),
            imei: new FormControl(""),
            bin: new FormControl(""),
            start_date: [this.start_date, Validators.required],

        });
        console.log('this.reportsForm : ', this.reportsForm)
        this.onStartDateSelect(this.start_date);
    }
    getBinsList() {
        this.deviceService.getAllBinsList().subscribe((data: any) => {
            // this.devices = data.data;
            this.binsArray = data.data;
            console.log("this.binsArray : ", this.binsArray);
        });
    }

    getReportDetails() {
        console.log('this.reportsForm.value : ', this.reportsForm.value)
        if (this.reportsForm.value.reportType == 'Vehicle Wise') {
            console.log('vehicle wise api hit')
            this.getReportByVehicle();
        }
        else {
            console.log('bin wise api hit');
            this.getReportByBin();
        }

    }

    getReportByVehicle() {
        let reportObj = {
            imei: this.imei,
            start_date: this.startDate,
            end_date: this.endDate
        }
        this.dateFlag = true;
        this.processingHttp = true;
        if (reportObj.imei != null && reportObj.start_date != null && reportObj.end_date != null) {

            console.log('Report obj', reportObj)
            this
                .reportsService
                .getReportById(reportObj)
                .subscribe((data: any) => {
                    this.reportDetails = data.data;
                    console.log('this.imei : ', this.imei)
                    console.log('this.start_date : ', this.imei)
                    this.reportDetails
                        .forEach(element => {
                            element
                                .vehicles
                                .forEach(ele => {
                                    // console.log('eleeleeleeleele', ele)
                                    // ele.In_time = moment(ele.In_time).format('MMMM Do YYYY,h:mm:ss a');
                                    // ele.out_time = moment(ele.out_time).format('MMMM Do YYYY,h:mm:ss a');
                                    // console.log('element.In_time', ele.In_time)
                                    // console.log('element.out_time', ele.out_time)
                                    this.spinnerService.hide();

                                });
                        });
                    this.processingHttp = false;
                    // console.log("reportsService details:", this.reportsService)
                }, (error) => {
                    this.processingHttp = false
                    this.spinnerService.hide();
                    this.reportDetails = [];
                });
        }
    }

    getReportByBin() {
        let reportObj = {
            bin: this.selectedBin,
            start_date: this.startDate,
            end_date: this.endDate
        }
        this.dateFlag = true;
        this.processingHttp = true;
        if (reportObj.bin != null && reportObj.start_date != null && reportObj.end_date != null) {

            console.log('Report obj', reportObj)
            this
                .reportsService
                .getReportByBin(reportObj)
                .subscribe((data: any) => {
                    this.reportDetails = data.data;
                    console.log('this.imei : ', this.imei)
                    console.log('this.start_date : ', this.imei)
                    this.reportDetails
                        .forEach(element => {
                            element
                                .vehicles
                                .forEach(ele => {
                                    console.log('eleeleeleeleele', ele)
                                    // ele.In_time = moment(ele.In_time).format('MMMM Do YYYY,h:mm:ss a');
                                    // ele.out_time = moment(ele.out_time).format('MMMM Do YYYY,h:mm:ss a');
                                    console.log('element.In_time', ele.In_time)
                                    console.log('element.out_time', ele.out_time)
                                    this.spinnerService.hide();

                                });
                        });
                    this.processingHttp = false;
                    // console.log("reportsService details:", this.reportsService)
                }, (error) => {
                    this.processingHttp = false
                    this.reportDetails = [];
                    this.spinnerService.hide();
                });
        }

    }
    getDeviceDetails() {
        this.deviceService.getAllDevice().subscribe((data: any) => {
            this.devices = data.data;
            console.log("deviceService", this.devices);
        });
    }

    selectReportType(report: any) {
        // this.spinnerService.show();
        // console.log('report form value', report)
        if (report.reportType == 'Vehicle Wise') {
            this.imei = report.imei;
        }
        else {
            this.selectedBin = report.bin;
        }
    }

    onStartDateSelect(event: any) {
        console.log("start date:", event, this.reportsForm)
        let start: any = moment(event).format('MM-DD-YYYY')
        var end: any = moment(start).format();
        let today: any = moment().format('MM-DD-YYYY');
        start = moment(start);
        today = moment(today);
        const difference = today.diff(start, 'days');
        // console.log('start : ', start,' today : ', today);
        console.log('difference : ', difference);
        start = moment(event);
        this.dateFlag = true;
        switch (true) {

            case difference === 0:
                {
                    console.log('Today\'s date');
                    end = new Date();
                    this.setBothDates(start, end);
                    break;
                }
            case difference > 0:
                {
                    end = start;
                    this.setBothDates(start, end)
                    break;
                }
            case difference < 0:
                {
                    console.log('Coming(Future date) : ', start, event)
                    this.start_date = new Date();
                    this.dateFlag = false;
                    break;
                }
            default:
                {
                    break;
                }
        }

    }

    setBothDates(start, end) {
        console.log('start is : ', start);
        this.startDate = moment(start).format('YYYY-MM-DD');
        this.endDate = moment(end).format('YYYY-MM-DD');
        console.log('start is : ', this.startDate);
        console.log('end is : ', this.endDate);

    }

    onEndDateSelect(event: any) {
        this.endDate = moment(event)
            .utc()
            .format();
    }
}
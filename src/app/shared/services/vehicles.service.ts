import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
// import { Observable } from 'rxjs/Rx';
import "rxjs/Rx";

// import 'rxjs/add/operator/map';
import { VehiclesModel } from "../../theme/pages/default/vehicles/vehicles.component.model";
import { Observable } from "rxjs/Observable";
@Injectable()
export class VehiclesService {
    constructor(private http: Http) { }

    /**
     * Get all Vehicle Details.
     * @return {Observable<Response>} response with user Data.
     */
    getAllVehicle(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/vehicle", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get all Vehicle Details.
     * @return {Observable<Response>} response with user Data.
     */
    getByQuery(query): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get Vehicle by pagination.
     * @return {Observable<Response>} response with user Data.
     */
    getByPagination(query, action, id): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/user?getBy=" + query + "&" + action + "=" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Save user data.
     * @param {VehicleModel} user user object
     * @return {Observable<Response>} response with user data.
     */

    createVehicle(vehicle: VehiclesModel) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const option = new RequestOptions({ headers: headers });
        return this.http
            .post("/vehicle", vehicle, option)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Get single Vehicle details.
     * @param id Vehicle id.
     * @return {Observable<Response>} current User data.
     */
    getVehicleById(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/vehicle" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Delete single Vehicle details.
     * @param id Vehicle id.
     * @return {Observable<Response>} deleted user data.
     */
    deleteVehicle(id: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .delete("/vehicle/" + id, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Update single user details.
     * @param user user updated Data.
     * @param id string user id.
     * @return {Observable<Response>} current status data.
     */
    updateVehicle(id: any, vehicle: any) {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .put("/vehicle/" + id, vehicle, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    /**
    * Get global search result.
    * @return {Observable<Response>} response with global search Data.
    */
    getGlobalSearch(search): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/vehicle/search?search=" + search, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    getBins() {
        //http://192.168.1.118:3032/api/geo
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .get("/geo", options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }


    /*
    vehicle/assign/553563563563987
    */

    /**
     * Update single user details.
     * @param user user updated Data.
     * @param id string user id.
     * @return {Observable<Response>} current status data.
     */
    assignBinToVehicle(id: any, assignObj: any) {
        console.log('PUT assignObj : ', assignObj)
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http
            .put("/vehicle/" + id, assignObj, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Error Handler.
     * @param {any} err error if any
     * @return {err} throw error if any
     */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

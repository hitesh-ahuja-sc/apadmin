import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ThemeComponent } from "./theme/theme.component";
import { LayoutModule } from "./theme/layouts/layout.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpModule } from "@angular/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ScriptLoaderService } from "./_services/script-loader.service";

import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
// import { TemplateTableComponent } from "./theme/pages/default/template-table/template-table.component";
import { SharedModule } from "./shared/shared.module";
import { CommonModule, HashLocationStrategy, Location, LocationStrategy } from "@angular/common";
import { ToastModule } from "ng2-toastr/ng2-toastr";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgCircleProgressModule } from 'ng-circle-progress';
// import { UsersComponent } from './theme/pages/users/users.component';
// import { AngularDateTimePickerModule } from "angular2-datetimepicker";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
    declarations: [ThemeComponent, AppComponent],
    // TemplateTableComponent
    imports: [
        LayoutModule,
        ToastModule.forRoot(),
        CommonModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        SharedModule,
        HttpModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        NgSelectModule,
        NgCircleProgressModule.forRoot()
        // AngularDateTimePickerModule
    ],
    providers: [ScriptLoaderService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
    bootstrap: [AppComponent]
})
export class AppModule { }

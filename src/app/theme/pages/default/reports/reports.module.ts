import { NgModule } from "@angular/core";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { ReportsComponent } from "./reports.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DefaultComponent } from "../default.component";
import { LayoutModule } from "../../../layouts/layout.module";
import { SharedModule } from "../../../../shared/shared.module";
import { AngularDateTimePickerModule } from "angular2-datetimepicker";
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastModule } from "ng2-toastr/ng2-toastr";
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: ReportsComponent,
                children: [
                    // { path: 'input', component: PrimeNgInputComponent },
                    // { path: 'button', component: PrimeNgButtonComponent },
                    // { path: 'panel', component: PrimeNgPanelComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        ToastModule.forRoot(),
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        // primeng modules
        AngularDateTimePickerModule,
        NgSelectModule,
        NgxSpinnerModule,
        NgbModule.forRoot()

    ],
    declarations: [
        ReportsComponent
        // PrimeNgInputComponent,
        // PrimeNgButtonComponent,
        // PrimeNgPanelComponent
    ]
})
export class ReportsModule {

}

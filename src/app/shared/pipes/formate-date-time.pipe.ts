/*
 * Format the given date time
 * Usage:
 *   value | formatDateTime
 * Example:
 *   {{ 2016-11-07T22:46:47.267 | formatDateTime }}
 *   formats to: date gets formatted to 11/07/2016 22:46:47
*/
import { Pipe, PipeTransform } from '@angular/core';
// import { DatePipe } from '@angular/common';

@Pipe(
    {
        name: 'formatDateTime'
    }
)

export class FormatDateTimePipe implements PipeTransform {
    constructor() { }
    transform(updatedDate: string): string {
        // console.log('formatDateTime : ', updatedDate)
        const array = updatedDate.split(':');
        let hour = Number(array[0]), minutes = Number(array[1]), seconds = Number(array[2]);
        // console.log('array : ', array);
        if (hour > 0) {
            // console.log('hour : ', hour);
            return hour + ' hour ' + minutes + ' minutes ' + seconds + ' seconds';
        } else if (minutes > 0) {
            // console.log('minutes : ', minutes);
            return minutes + ' minutes ' + seconds + ' seconds';
        }
        else {
            // console.log('seconds : ', seconds);
            return seconds + ' seconds';
        }
    }
}
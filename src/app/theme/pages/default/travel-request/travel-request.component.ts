import {
    Component,
    OnInit,
    ViewEncapsulation,
    ViewContainerRef
} from "@angular/core";
import { UserService } from "../../../../shared/services/user.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { FileUploadService } from "../../../../shared/services/file-upload.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { RequestService } from "../../../../shared/services/request.service";

@Component({
    selector: "app-users",
    templateUrl: "./travel-request.component.html",
    styleUrls: ["./travel-request.component.scss"]
})
export class TravelRequestComponent implements OnInit {
    users: any = [];
    // adminData: any = [];
    addUserForm: FormGroup;
    editForm: FormGroup;
    selectedUser: any = {};
    filteredItems: any = [];
    items: any = [];
    inputName: string = "";
    previous: string;
    next: string;
    index;
    requestDetails: any = [];
    currentId: string;
    amount: number;
    allTransactions: any = [];
    constructor(
        private spinnerService: NgxSpinnerService,
        private requestService: RequestService,
        private fb: FormBuilder,
        public toastr: ToastsManager,
        vcr: ViewContainerRef,
        private fileUploadService: FileUploadService
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }
    ngOnInit() {
        this.spinnerService.show();
        this.getUserDetails();
        this.addUserForm = this.fb.group({
            user_name: [null, Validators.compose([Validators.required, Validators.maxLength(24)])],
            user_mobile: [
                null,
                Validators.compose([Validators.required, Validators.maxLength(10)])
            ],
            user_password: [null, Validators.compose([Validators.required, Validators.maxLength(8)])],
            // user_role: [null, Validators.required],
            user_email: [null, Validators.required],
        });

        this.editForm = this.fb.group({

            name: [null, Validators.compose([Validators.required, Validators.maxLength(24)])],
            mobile: [null, Validators.compose([Validators.required])],
            email: [null, Validators.required],
        });

    }
    approveUserRequest(amount) {
        this.requestService.approveRequest(amount, this.currentId).subscribe((approved) => {
            if (approved) {
                document.location.reload();
            }
        });
    }

    rejectUserRequest() {


        this.requestService.rejectUserRequest(this.currentId).subscribe((reject) => {
            if (reject) {
                document.location.reload();
            }
        });
    }

    cardPage() {
        window.open("https://edenredcards.co.in", "_blank");
    }

    approveRequest(id: string) {
        this.currentId = id;

    }
    getUserDetails() {
        this.items = []
        this.requestService.allTravelRequestPendingService().subscribe((data: any) => {
            this.requestDetails = data.data;

            this.spinnerService.hide();
        }, (error) => {
            this.spinnerService.hide();
        });

        this.requestService.allTravelRequestTransaction().subscribe((data: any) => {

            this.allTransactions = data.data;
            this.allTransactions = this.allTransactions.filter(function(e) { return e.status !== 'pending' });

            this.spinnerService.hide();
        }, (error) => {
            this.spinnerService.hide();
        });
    }


    openModal(user: any, index) {
        this.index = index;

        // console.log('index is', index)
        if (user) {
            this.selectedUser = user;
            console.log(user);
            this.editForm = this.fb.group({
                name: [this.selectedUser.name, Validators.required],
                mobile: [this.selectedUser.mobile, Validators.compose([Validators.required, Validators.maxLength[10]])],
                email: [this.selectedUser.email, Validators.required]
            });
        }
    }

}

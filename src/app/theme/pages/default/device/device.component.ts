import {
    Component,
    OnInit,
    ViewEncapsulation,
    ViewContainerRef
} from "@angular/core";
import { DeviceService } from "../../../../shared/services/device.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { FileUploadService } from "../../../../shared/services/file-upload.service";
import { UserService } from "../../../../shared/services/user.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: "app-vehicles",
    templateUrl: "./device.component.html",
    styleUrls: ["./device.component.scss"]
})
export class DeviceComponent implements OnInit {
    devices: any = [];
    // adminData: any = [];
    addDeviceForm: FormGroup;
    editForm: FormGroup;
    userNumbers: any = [];
    selectedDevice: any = {};
    filteredItems: any = [];
    items: any = [];
    inputName: string = "";
    previous: string;
    next: string;
    index;
    deviceDetails: any = [];
    userDetails: any = [];
    constructor(
        private spinnerService: NgxSpinnerService,
        private deviceService: DeviceService,
        private userService: UserService,
        private fb: FormBuilder,
        public toastr: ToastsManager,
        vcr: ViewContainerRef,
        private fileUploadService: FileUploadService
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }
    ngOnInit() {
        this.spinnerService.show();
        this.getDeviceDetails();
        this.addDeviceForm = this.fb.group({
            imei: [null, Validators.required],
            deviceType: [null, Validators.required],
            userNumber: [null, Validators.required],
            vehicleNumber: [null, Validators.required],
            chassisNumber: [null, Validators.required],
        });
        this.getUserDetails();


        this.editForm = this.fb.group({
            imei: [null, Validators.required],
            deviceType: [null, Validators.required],
            userNumber: [null, Validators.required],
            vehicle_number: [null, Validators.compose([Validators.required])],
            chassis_number: [null, Validators.required],
        });
    }

    getDeviceDetails() {
        this.items = []
        this.deviceService.getAllDevice().subscribe((data: any) => {
            this.deviceDetails = data.data;
            this.spinnerService.hide();
            // this.previous = data.result.previous;
            // this.next = data.result.next;
            // this.items = [];
            // this.adminDetails = [];
            console.log("device details:", this.deviceDetails)
            this.items = this.deviceDetails;
            console.log("device data is :", this.items);
        }, (error) => {
            this.spinnerService.hide();
        });
    }

    FilterByName() {
        this.filteredItems = [];
        console.log("input name", this.inputName);
        console.log(this.deviceDetails);
        if (this.inputName != "") {
            this.deviceDetails.forEach(element => {
                if (
                    element.vehicleNumber.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    0
                    // ||
                    // element.imei.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    // 0 ||
                    // element.chassisNumber.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    //  0 || 
                    // element.deviceType.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    // 0 ||
                    // element.userNumber.name.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    //  0 ||
                    //  element.userNumber.mo.toUpperCase().indexOf(this.inputName.toUpperCase()) >=
                    //  0 

                ) {
                    this.filteredItems.push(element);
                }
            });
        } else {
            this.filteredItems = this.deviceDetails;
        }
        console.log(this.filteredItems);
        this.items = this.filteredItems
    }

    previousPage() {
        console.log("previous", this.previous);
        this.items = [];
        this.deviceService.getByPagination("admin", "previous", this.previous).subscribe((data: any) => {
            if (data.result.previous !== null) {
                this.deviceDetails = data.result.data;
                console.log('items after previous', this.deviceDetails)
                this.items = this.deviceDetails;
                this.previous = data.result.previous;
                this.next = data.result.next;
                console.log("previous:" + this.previous + " next: " + this.next)
            }
            else {
                this.deviceDetails = data.result.data;
                this.items = this.deviceDetails;
                this.next = data.result.next;
                this.previous = null;
                console.log("previous:" + this.previous + " next: " + this.next)
            }
        });
    }
    getUserDetails() {

        this.userService.getAllUser().subscribe((data: any) => {
            this.userDetails = data.data;
            console.log("User details:", this.userDetails)
        });
    }
    nextPage() {
        console.log("next", this.next);
        this.items = [];
        this.deviceDetails = [];
        this.deviceService.getByPagination("admin", "next", this.next).subscribe((data: any) => {
            if (data.result.next !== null) {
                this.deviceDetails = data.result.data;
                console.log('items after next', this.deviceDetails);
                this.items = this.deviceDetails;
                this.previous = data.result.previous;
                this.next = data.result.next;
                console.log("previous:" + this.previous + " next: " + this.next)
            } else {
                this.deviceDetails = data.result.data;
                this.items = this.deviceDetails;
                this.previous = data.result.previous;
                this.next = null
                console.log("previous:" + this.previous + " next: " + this.next)
            }
        });
    }

    addDevice(device) {
        let newDevice: any = {
            deviceType: device.deviceType,
            imei: device.imei,
            userNumber: device.userNumber,
            vehicle_number: device.vehicleNumber,
            chassis_number: device.chassisNumber,
        };
        this.deviceService.createDevice(newDevice).subscribe(
            data => {
                setTimeout(() => {
                    console.log("new device data:==>", data);
                    // this.getDeviceDetails();
                    this.items.push(data.data);
                    this.toastr.success("Device Added Succesfully", "!Added");
                }, 1000);
            },
            (error: any) => {
                const err = JSON.parse(error);
                console.log("Error:", err.message);
                this.toastr.error(err.message, "Error");
                this.getDeviceDetails();
            }
        );
        console.log("Created device is:", newDevice);
    }

    openModal(device: any, index) {
        this.index = index;
        if (device) {
            this.selectedDevice = device;
            console.log(device);
            this.editForm = this.fb.group({
                deviceType: [this.selectedDevice.deviceType, Validators.required],
                imei: [this.selectedDevice.imei, Validators.required],
                userNumber: [this.selectedDevice.userNumber.mobile, Validators.required],
                vehicle_number: [this.selectedDevice.vehicle_number, Validators.required],
                chassis_number: [this.selectedDevice.chassis_number, Validators.required]
            });
        }
    }

    editDeviceDetails(modalData: any) {
        modalData.vehicle_number = modalData.vehicle_number.toString();
        let updateDevice = {};
        console.log("ModalData will be:", modalData);

        setTimeout(() => {
            updateDevice = {
                deviceType: modalData.deviceType,
                imei: modalData.imei,
                userNumber: modalData.userNumber,
                vehicle_number: modalData.vehicle_number,
                chassis_number: modalData.chassis_number,
            };
            this.deviceService
                .updateDevice(this.selectedDevice._id, updateDevice)
                .subscribe(
                data => {
                    this.toastr.success("device updated successfully", "Updated!");
                    // this.getDeviceDetails();
                    this.items.splice(this.index, 1, data.data)
                },
                (error: any) => {
                    const err = JSON.parse(error);
                    this.toastr.error(err.message, "Error");
                    this.getDeviceDetails();
                }
                );
        }, 1000);
    }

    viewDeviceProfile(device: any) {
        this.selectedDevice = device;
        console.log(this.selectedDevice);
    }

    deleteDevice() {
        console.log(this.selectedDevice);
        setTimeout(() => {
            this.deviceService.deleteDevice(this.selectedDevice._id).subscribe(data => {
                this.toastr.success("device deleted successfully", "Deleted!");
                // this.getDeviceDetails();
                this.items.splice(this.index, 1);
            });
        }, 1000);
    }

    clearField() {
        this.addDeviceForm.reset();
    }

}

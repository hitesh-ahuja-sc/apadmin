import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";
@Injectable()
export class MapService {
    constructor(private http: Http) { }

    /**
     * Get all device Details.
     * @return {Observable<Response>} response with device Data.
     */
    getDeviceLocation(imeiobj): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http.post("/location/location", imeiobj, options).map((res: Response) => res.json()).catch(this.handleError);
    }

    getDeviceRouteReplay(obj): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        const options = new RequestOptions({ headers: headers });
        return this.http.post("/location/info", obj, options)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    handleError(err: Response | any) {
        return Observable.throw(err._body);
    }
}

import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
@Injectable()
export class LocationService {
    constructor(private http: Http) { }

    /**
     * Get all User Locations.
     * @return {Observable<Response>} response with user Data.
     */
    getAllUserLocations(): Observable<Response> {
        const headers = new Headers({ "Content-Type": "application/json" });
        // headers.append("Authorization", "jwt " + localStorage.getItem("token"));
        // const options = new RequestOptions({ headers: headers });
        return this.http
            .get("https://api.police2.fortunekit.com/api/user/getAll/locations")
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    /**
     * Error Handler.
     * @param {any} err error if any
     * @return {err} throw error if any
     */
    handleError(err: any) {
        return Observable.throw(err._body);
    }
}

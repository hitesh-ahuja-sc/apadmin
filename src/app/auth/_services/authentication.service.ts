import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class AuthenticationService {

    constructor(private http: Http) {
    }

    login(credential) {
        return this.http.post('/authenticate', credential)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let userProfile = response.json();
                if (userProfile && userProfile.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(userProfile));
                    localStorage.setItem('role', userProfile.user.role);
                    localStorage.setItem('token', userProfile.token);
                }
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
    }
    signIn(context) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post('/authenticate', context, { headers: headers }).map((res: Response) => res.json())
            .catch(this.handleError);
    }
    handleError(err: Error) {
        return Observable.throw(err);
    }
    // otpVerification(userOtp: any) {
    //     const headers = new Headers({ 'Content-Type': 'application/json' });
    //     return this.http.post('/smslogin/verifyotp', userOtp, { headers: headers }).map((res: Response) => res.json())
    //         .catch(this.handleError);
    // }
    // reOtpVerification(userOtp: any) {
    //     const headers = new Headers({ 'Content-Type': 'application/json' });
    //     return this.http.post('/smslogin/resendotp', userOtp, { headers: headers }).map((res: Response) => res.json())
    //         .catch(this.handleError);
    // }
}
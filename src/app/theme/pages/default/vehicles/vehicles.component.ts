import { Component, OnInit, ViewEncapsulation, Input, ViewContainerRef } from "@angular/core";
import { VehiclesService } from "../../../../shared/services/vehicles.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { FileUploadService } from "../../../../shared/services/file-upload.service";

/*third party plugins*/

import * as _ from 'lodash';

@Component({ selector: "app-vehicles", templateUrl: "./vehicles.component.html", styleUrls: ["./vehicles.component.scss"] })

export class VehiclesComponent implements OnInit {
    vehicles: any = [];
    // adminData: any = [];
    addVehicleForm: FormGroup;
    editForm: FormGroup;
    selectedVehicle: any = {};
    filteredItems: any = [];
    items: any = [];
    inputName: string = "";
    binList = [];
    previous: string;
    next: string;
    vehicleDetails: any = [];
    @Input() refreshBins: any = {
        name: Math.random()
    };
    constructor(private vehicleService: VehiclesService, private fb: FormBuilder, public toastr: ToastsManager, vcr: ViewContainerRef, private fileUploadService: FileUploadService) {
        this
            .toastr
            .setRootViewContainerRef(vcr);
    }
    ngOnInit() {
        this.getVehicleDetails();
        this.addVehicleForm = this
            .fb
            .group({
                vehicle_number: [
                    null, Validators.required
                ],
                chassis_number: [null, Validators.required]
            });

        this.editForm = this
            .fb
            .group({
                vehicle_number: [
                    null, Validators.compose([Validators.required])
                ],
                chassis_number: [null, Validators.required]
            });
        this.getBins();

    }

    getVehicleDetails() {
        this.items = []
        this
            .vehicleService
            .getAllVehicle()
            .subscribe((data: any) => {
                this.vehicleDetails = data.data;
                // this.previous = data.result.previous; this.next = data.result.next;
                // this.items = []; this.adminDetails = [];
                console.log("Vehicle details:", this.vehicleDetails)
                this.items = this.vehicleDetails;
                console.log("vehicle data is :", this.items);
            });
    }

    FilterByName() {
        this.filteredItems = [];
        console.log("input name", this.inputName);
        console.log(this.vehicleDetails);
        if (this.inputName != "") {
            this
                .vehicleDetails
                .forEach(element => {
                    if (element.name.toUpperCase().indexOf(this.inputName.toUpperCase()) >= 0 || element.mobile.toUpperCase().indexOf(this.inputName.toUpperCase()) >= 0 || element.email.toUpperCase().indexOf(this.inputName.toUpperCase()) >= 0) {
                        this
                            .filteredItems
                            .push(element);
                    }
                });
        } else {
            this.filteredItems = this.vehicleDetails;
        }
        console.log(this.filteredItems);
        this.items = this.filteredItems
    }

    previousPage() {
        console.log("previous", this.previous);
        this.items = [];
        this
            .vehicleService
            .getByPagination("admin", "previous", this.previous)
            .subscribe((data: any) => {
                if (data.result.previous !== null) {
                    this.vehicleDetails = data.result.data;
                    console.log('items after previous', this.vehicleDetails)
                    this.items = this.vehicleDetails;
                    this.previous = data.result.previous;
                    this.next = data.result.next;
                    console.log("previous:" + this.previous + " next: " + this.next)
                } else {
                    this.vehicleDetails = data.result.data;
                    this.items = this.vehicleDetails;
                    this.next = data.result.next;
                    this.previous = null;
                    console.log("previous:" + this.previous + " next: " + this.next)
                }
            });
    }

    nextPage() {
        console.log("next", this.next);
        this.items = [];
        this.vehicleDetails = [];
        this
            .vehicleService
            .getByPagination("admin", "next", this.next)
            .subscribe((data: any) => {
                if (data.result.next !== null) {
                    this.vehicleDetails = data.result.data;
                    console.log('items after next', this.vehicleDetails);
                    this.items = this.vehicleDetails;
                    this.previous = data.result.previous;
                    this.next = data.result.next;
                    console.log("previous:" + this.previous + " next: " + this.next)
                } else {
                    this.vehicleDetails = data.result.data;
                    this.items = this.vehicleDetails;
                    this.previous = data.result.previous;
                    this.next = null
                    console.log("previous:" + this.previous + " next: " + this.next)
                }
            });
    }

    addVehicle(vehicle: any) {
        let newVehicle: any = {
            vehicle_number: vehicle.vehicle_number,
            chassis_number: vehicle.chassis_number
        };
        this
            .vehicleService
            .createVehicle(newVehicle)
            .subscribe(data => {
                setTimeout(() => {
                    console.log("new user data:==>", data);
                    this.getVehicleDetails();
                    this
                        .toastr
                        .success("Vehicle Added Succesfully", "!Added");
                }, 1000);
            }, (error: any) => {
                const err = JSON.parse(error);
                console.log("Error:", err.message);
                this
                    .toastr
                    .error(err.message, "Error");
                this.getVehicleDetails();
            });
        console.log("Created User is:", newVehicle);
    }

    openModal(vehicle: any) {
        console.log('vehicle : ', vehicle._id);
        this.refreshBins = { vehicle: vehicle, bins: this.binList };
        // if (vehicle) {
        //     this.selectedVehicle = vehicle;
        //     console.log(vehicle);
        //     this.editForm = this
        //         .fb
        //         .group({
        //             vehicle_number: [
        //                 this.selectedVehicle.vehicle_number, Validators.required
        //             ],
        //             chassis_number: [this.selectedVehicle.chassis_number, Validators.required]
        //         });
        // }
    }

    editVehicleDetails(modalData: any) {
        modalData.vehicle_number = modalData
            .vehicle_number
            .toString();
        let updateVehicle = {};
        console.log("ModalData will be:", modalData);

        setTimeout(() => {
            updateVehicle = {
                vehicle_number: modalData.vehicle_number,
                chassis_number: modalData.chassis_number
            };
            this
                .vehicleService
                .updateVehicle(this.selectedVehicle._id, updateVehicle)
                .subscribe(data => {
                    this
                        .toastr
                        .success("Vehicle updated successfully", "Updated!");
                    this.getVehicleDetails();
                }, (error: any) => {
                    const err = JSON.parse(error);
                    this
                        .toastr
                        .error(err.message, "Error");
                    this.getVehicleDetails();
                });
        }, 1000);
    }

    viewVehicleProfile(vehicle: any) {
        this.selectedVehicle = vehicle;
        console.log(this.selectedVehicle);
    }

    deleteVehicle() {
        console.log(this.selectedVehicle);
        setTimeout(() => {
            this
                .vehicleService
                .deleteVehicle(this.selectedVehicle._id)
                .subscribe(data => {
                    this
                        .toastr
                        .success("Vehicle deleted successfully", "Deleted!");
                    this.getVehicleDetails();
                });
        }, 1000);
    }

    clearField() {
        this
            .addVehicleForm
            .reset();
    }

    getBins() {
        this
            .vehicleService
            .getBins()
            .subscribe((response) => {
                console.log('response : ', response.data);
                this.binList = response.data;

            }, (error) => {
                console.log('error : ', error)
            })
    }
    updateBins(event) {
        console.log('event : ', event)
        _.find(this.vehicleDetails, (vehicle) => {
            if (vehicle.imei == event.imei) {
                vehicle.binid = event.binid;
                console.log('matched : ', vehicle)
            }
        })
    }

}

import { Directive, ElementRef, Input } from '@angular/core';
import * as _ from 'lodash';

@Directive({
    selector: '[roles]' // Attribute selector
})
export class RolesDirective {
    @Input('viewAccess') access: string;
    @Input('restrict') restrict: string;
    constructor(private el: ElementRef) {
        // console.log("value: - " + this.access);

        // if (this.access == "1") {
        //     el.nativeElement.style.backgroundColor = 'yellow';
        // }
        // console.log('Hello RolesDirective Directive');
    }
    ngOnInit() {
        this.el.nativeElement.style.display = "none";
        // console.log("roles : ", this.access)

        // console.log("parse : ", JSON.parse(this.access))
        let roles = this.access !== undefined ? JSON.parse(this.access) : undefined;

        let restrict = this.restrict !== undefined ? JSON.parse(this.restrict) : undefined;
        let role = localStorage.getItem('role');
        // console.log("access role is : ", this.access)
        if (roles != undefined) {
            // let activeRole = this.access.split(" ");
            let self = this;
            if (roles.length > 0) {
                _.forEach(roles, function(ele, i) {
                    // console.log("count is : ", i)
                    // console.log("role in loop is :", ele)
                    if (ele == role) {
                        // self.el.nativeElement.style.display = "visible";
                        // console.log("role is : ", role)
                        self.el.nativeElement.style.display = "initial";
                        // console.log(" self.el.nativeElement.style", self.el.nativeElement.style)
                        return false;
                    }
                    // else if (role == 'individual') {
                    //     // console.log("matched", ele)
                    //     self.el.nativeElement.style.pointerEvents = "none";
                    //     self.el.nativeElement.style.opacity = "0.2";
                    //     return false;
                    // }
                    else {
                        // console.log("role not found : ", ele)
                        self.el.nativeElement.style.display = "none";
                    }
                })
            }
            // else {
            //     if (activeRole[0] != role) {
            //         // console.log("not matched is : ", activeRole);
            //         this.el.nativeElement.style.display = "none";
            //     }
            // }
        }
        if (restrict != undefined) {

            // let activeRole = this.restrict.split(" ");
            let self = this;
            if (restrict.length > 0) {
                _.forEach(restrict, function(ele, i) {
                    // console.log("count is : ", i)
                    // console.log("role in loop is :", ele)
                    if (ele == role) {
                        // self.el.nativeElement.style.display = "visible";
                        // console.log("role is : ", role)
                        self.el.nativeElement.disabled = true;
                        self.el.nativeElement.style.pointerEvents = "none";
                        self.el.nativeElement.style.opacity = "0.5";
                        console.log(" self.el.nativeElement.style", self.el);
                        return false;
                    }
                    else {
                        // console.log("role not found : ", ele)
                        self.el.nativeElement.style.display = "initial";
                    }
                })
            }
            // else {
            //     if (activeRole[0] != role) {
            //         // console.log("in restrict not matched is : ", activeRole);
            //         this.el.nativeElement.style.display = "none";
            //     }
            // }
        }

    }



}

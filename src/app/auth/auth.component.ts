import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AuthenticationService } from './_services/authentication.service';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { AlertComponent } from './_directives/alert.component';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-2.component.html',
    styleUrls: ['./templates/login-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    loginForm: FormGroup;
    otpForm: FormGroup;
    submitFlag: boolean;
    // loginFlag: boolean;
    userid;
    user;
    // resendOtpFlag: boolean;
    @ViewChild('alertSignin',
        { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertSignup',
        { read: ViewContainerRef }) alertSignup: ViewContainerRef;
    @ViewChild('alertForgotPass',
        { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;

    constructor(
        private spinnerService: NgxSpinnerService,
        private _router: Router,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _authService: AuthenticationService,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver,
        private fb: FormBuilder) {
    }

    ngOnInit() {
        this.model.remember = true;
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._router.navigate([this.returnUrl]);
        this.submitFlag = true;
        // this.loginFlag = false;
        // this.resendOtpFlag = false;
        this.loginForm = this.fb.group({
            email: [null],
            password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
        });
        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/default/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });
    }

    getCurrentPosition() {
        let currentLocation: any = {};
        currentLocation = { lat: 19.2403, long: 73.1305 }
        localStorage.setItem('currentLocation', JSON.stringify(currentLocation))
        navigator.geolocation.getCurrentPosition(position => {
            console.log("position : ", position);
            const lat = position.coords.latitude;
            const long = position.coords.longitude;
            currentLocation = { lat: lat, long: long }
            localStorage.setItem('currentLocation', JSON.stringify(currentLocation))
        });

    }

    submit(credentials) {
        // console.log('mobile', mobile);
        // this.mobStatus = false;
        this.spinnerService.show();
        this._authService.signIn(credentials).subscribe((userData: any) => {
            this.userid = userData.userId;
            console.log('context is:', credentials);
            console.log('User data is:', userData);

            setTimeout(() => {
                console.log(userData);
                if (userData && userData.token) {
                    localStorage.setItem('currentUser', JSON.stringify(userData.user));
                    localStorage.setItem('role', userData.user.role);
                    localStorage.setItem('token', userData.token);
                    // this.resendOtp();
                    if (userData.statusCode === 200) {
                        this.spinnerService.hide();
                    }
                    this._router.navigate(['/user']);
                }
            }, 1000);
        }, (error) => {
            console.log('Error:', error);
            this.showAlert('alertSignin');
            let errorResponse = error.json();
            console.log("error is : ", error.json());
            this.spinnerService.hide();

            if (errorResponse.statusCode == 401 || errorResponse.statusCode == 400 || errorResponse.statusCode == 404) {
                this._alertService.error(errorResponse.message);
            } else {
                this._alertService.error('Please check your internet connection !!');
            }
            // switch (errorResponse.statusCode) {
            //     case 404: {
            //         console.log("404 : ", errorResponse)
            //         this._alertService.error(errorResponse.message);
            //         break;
            //     }
            //     case 401: {
            //         console.log("401 : ", errorResponse)
            //         this._alertService.error(errorResponse.message);
            //         break;
            //     }
            //     // default:{
            //     //     console.log("404 : ",errorResponse)
            //     //     this._alertService.error("Please check your internet connection !!");
            //     //     break;
            //     // }

            // }
            this.loading = false;

            // this.resendOtpFlag = false;
        });

    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }



    // signin() {
    //     this.loading = true;
    //     let credential = {
    //         'email': this.model.email,
    //         'password': this.model.password
    //     }

    //     // console.log('credentials:', credential)
    //     this._authService.login(credential).subscribe(
    //         data => {
    //             let user = JSON.parse(localStorage.getItem('currentUser'));
    //             let role = user.user.role;
    //             console.log('role', role);
    //             // if (role == 'admin') {

    //             this._router.navigate([this.returnUrl]);
    //             // }
    //         },
    //         error => {
    //             this.showAlert('alertSignin');
    //             let errorResponse = error.json();
    //             console.log("error is : ", error.json());
    //             switch (errorResponse.statusCode) {
    //                 case 404: {
    //                     console.log("404 : ", errorResponse)
    //                     this._alertService.error(errorResponse.message);
    //                     break;
    //                 }
    //                 case 401: {
    //                     console.log("401 : ", errorResponse)
    //                     this._alertService.error(errorResponse.message);
    //                     break;
    //                 }
    //                 // default:{
    //                 //     console.log("404 : ",errorResponse)
    //                 //     this._alertService.error("Please check your internet connection !!");
    //                 //     break;
    //                 // }

    //             }
    //             this.loading = false;
    //         });
    // }

    // signup() {
    //     this.loading = true;
    //     this._userService.create(this.model).subscribe(
    //         data => {
    //             this.showAlert('alertSignin');
    //             this._alertService.success(
    //                 'Thank you. To complete your registration please check your email.',
    //                 true);
    //             this.loading = false;
    //             LoginCustom.displaySignInForm();
    //             this.model = {};
    //         },
    //         error => {
    //             this.showAlert('alertSignup');
    //             this._alertService.error(error);
    //             this.loading = false;
    //         });
    // }

    // forgotPass() {
    //     this.loading = true;
    //     this._userService.forgotPassword(this.model.email).subscribe(
    //         data => {
    //             this.showAlert('alertSignin');
    //             this._alertService.success(
    //                 'Cool! Password recovery instruction has been sent to your email.',
    //                 true);
    //             this.loading = false;
    //             LoginCustom.displaySignInForm();
    //             this.model = {};
    //         },
    //         error => {
    //             this.showAlert('alertForgotPass');
    //             this._alertService.error(error);
    //             this.loading = false;
    //         });
    // }

    // }


    //     this._authService.reOtpVerification(userOtp).subscribe((userData: any) => {
    //         this.user = userData.data;
    //         console.log('User data:', userData);
    //         setTimeout(() => {
    //             this.resendOtpFlag = true;
    //         }, 10000);
    //     });
    // }
}
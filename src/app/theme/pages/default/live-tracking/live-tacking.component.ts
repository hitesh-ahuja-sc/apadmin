import { Component, OnInit, OnChanges, ViewEncapsulation, ViewContainerRef, ViewChild, ElementRef } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from "@angular/forms";
import { DeviceService } from "../../../../shared/services/device.service";
import { MapService } from "../../../../shared/services/map.service";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { NgxSpinnerService } from 'ngx-spinner';

declare var google: any;
import * as $ from "jquery";
import * as _ from "lodash";
import * as moment from 'moment';
import { elementAt } from "rxjs/operator/elementAt";
declare var GMaps: any;

@Component({
    selector: "app-map",
    templateUrl: "./live-tacking.component.html",
    styleUrls: ["./live-tacking.component.scss"]
})
export class LiveTrackingComponent implements OnInit, OnChanges {
    // name: udar likha tha
    map;
    lat;
    infowindow: any;
    long;
    interval;
    key: "AIzaSyAD3BujxZbC4o8V7WijuOxZ8x5E4zLpMQM";
    vehicleDetails;
    devices: any = [];
    vehicleForm: FormGroup;
    latlng: any = {};
    liveLocation;
    ignition;
    time;
    speed;
    trackingData = [];
    currentLocation: any = {};
    flag: boolean = false;
    marker;
    markers = [];
    infoWindows: any = [];
    shapes = [];
    infoWinfowForPolygon = new google.maps.InfoWindow;
    constructor(
        private spinnerService: NgxSpinnerService,
        private fb: FormBuilder,
        public toastr: ToastsManager,
        vcr: ViewContainerRef,
        private deviceService: DeviceService,
        private mapService: MapService
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.spinnerService.hide();
        this.getDeviceDetails();
        this.getCurrentPosition();
        this.selectVehicle();
        // setTimeout(() => {
        this.init();
        // }, 3000)

    }
    ngOnDestroy() {
        console.log('Before live-tracking ngOnDestroy : ', this.interval);
        clearInterval(this.interval);
        console.log('After live-tracking ngOnDestroy : ', this.interval);
    }

    ngOnChanges() {
    }
    timeObj: any = {
        start: moment()
            .startOf("day")
            .toDate(),
        end: moment()
            .endOf("day")
            .toDate()
    };
    date: Date = new Date();
    settings = {
        bigBanner: false,
        timePicker: false,
        format: "dd-MM-yyyy",
        defaultOpen: false
    };

    getDeviceDetails() {
        this.deviceService.getAllDevice().subscribe((data: any) => {
            this.devices = data.data;
            const imeiArray = _.map(this.devices, 'imei');
            // console.log("imeiArray : ", imeiArray);
            // console.log("deviceService ", this.devices)
            this.vehicleForm.patchValue({ imei: imeiArray });

            this.vehicleTracking({ imei: imeiArray });
        });
    }


    selectVehicle() {
        this.vehicleForm = this.fb.group({
            imei: [],
        });
    }

    init() {
        console.log('this.currentLocation : ', this.currentLocation, document.getElementById("map"))
        if (this.currentLocation) {
            this.map = new google.maps.Map(document.getElementById("map"), {
                zoom: 12,
                center: new google.maps.LatLng(
                    this.currentLocation.lat,
                    this.currentLocation.long
                ),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
        console.log('this.map', this.map)
    }
    placeMarker() {
        this.spinnerService.hide();
        // var infowindow = new google.maps.InfoWindow();
        let self = this;
        console.log('markersList : ', this.markers)
        for (let i = 0; i < this.trackingData.length; i++) {
            const device = _.find(this.devices, (ele) => {
                return ele.imei == this.trackingData[i].imei
            })
            // console.log('++++ device ++++ : ', device)
            if (this.trackingData[i].result !== null) {
                this.infowindow = new google.maps.InfoWindow(
                    {
                        content: device.vehicle_number
                    }
                );
                // console.log("this.trackingData result[ " + i + " ] : ", this.trackingData[i].result)
                var center = new google.maps.LatLng(this.trackingData[i].result.lat, this.trackingData[i].result.long);
                // this.map.panTo(center);
                this.marker = new google.maps.Marker({
                    position: new google.maps.LatLng(
                        this.trackingData[i].result.lat,
                        this.trackingData[i].result.long
                    ),
                    animation: google.maps,
                    map: this.map,
                    title: this.trackingData[i].result.vehicleNumber
                });

                self.markers.push(self.marker);
                // let self=this;
                google.maps.event.addListener(self.markers[i], 'click', function() {
                    // console.log('self.markers[i] : ', self.markers[i])
                    self.infowindow.open(self.map, self.markers[i]);
                    // this.map.setZoom(9);
                    // this.map.setCenter(self.marker.getPosition());
                });

                self.infoWindows.push(self.infowindow);
                self.infowindow.open(self.map, self.markers[i]);
                let data: any = this.trackingData[i];
                let marker = self.marker;
                google.maps.event.addListener(self.marker, "click", (function(marker, i) {
                    return function() {
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                        self.infowindow.setContent("<div style = 'width:95px;min-height:15px'>" + data.vehicleNumber + "</div>");
                        self.infowindow.open(self.map, marker);
                    }
                })(marker, i));


            }
            else {
                this.spinnerService.hide();
                this.toastr.error("data not found", this.trackingData[i].imei)
                //this.clearInfoWindow();
            }
        }
    }
    clearInfoWindow() {
        this.infoWindows.forEach((ele) => {
            // console.log('info window', ele);
            ele.close(this.map, this.marker);
            ele.setContent('');
        })
        this.infoWindows = [];
    }
    clearMarkers() {
        this.markers.forEach((marker) => {
            console.log('marker : ', marker)
            marker.setMap(null);
        })
        this.markers = [];


    }

    clearShapes() {
        this.shapes.forEach((shape) => {
            shape.setMap(null)
        })
        this.shapes = [];
    }
    vehicleTracking(imei) {
        console.log('imei : ', imei);

        this.spinnerService.show();
        clearInterval(this.interval);
        let body: any = {
            imei: imei.imei
        }
        let self = this;
        this.findVehicleAndSetIcon(imei.imei);
        this.mapService.getDeviceLocation(body).subscribe(
            (response: any) => {
                this.trackingData = [];
                _.forEach(response.data, (data) => {
                    if (data.result !== null) {
                        data.result.ignitionStatus = data.result.ignition == true ? 'On' : 'Off';
                        self.trackingData.push(data)
                    }
                    else {
                        this.spinnerService.hide();
                        self.toastr.error("Device not found", data.imei);
                    }
                });
                this.clearMarkers()
                this.clearInfoWindow();
                setTimeout(() => {
                    this.placeMarker();
                }, 300)
                this.getDeviceLocationByInterval(imei);
            }, (error) => {
                // this.infowindow.close(this.map, this.marker);
                this.trackingData = []
                this.spinnerService.hide();
                this.clearMarkers();
                this.clearInfoWindow();
            }
        );
        this.flag = false;
        this.findVehicleAndSetIcon(imei.imei)
    }

    findVehicleAndSetIcon(imei) {
        this.clearShapes();
        if (imei && imei.length > 0) {
            imei.forEach((imei) => {
                // console.log('imei : ',imei);
                const matchedObj = _.find(this.devices, { imei: imei })
                // console.log('matchedObj : ',matchedObj);
                if (matchedObj.vehicle && matchedObj.vehicle.binid) {
                    // console.log('matchedObj.vehcile : ', matchedObj.vehicle)
                    const color = this.getRandomeColor();
                    localStorage.setItem('color', color)
                    matchedObj.vehicle.binid.forEach((binObj) => {
                        // console.log('binObj : ',binObj) const coordsObj=Object.assign({},binObj);
                        this.checkkGeofenceType(binObj)
                    });
                }
            })
        }

    }

    checkkGeofenceType(bin) {
        switch (bin.loc.type.toLowerCase()) {
            case 'polygon':
                {
                    // console.log('Bin typpe is polygon : ', bin.loc.coordinates)
                    // const coordinates=bin.loc.coordinates[0];
                    const latLngArray = this.setLatLongInObj(bin.loc.coordinates[0]);
                    // console.log('latLngArray : ', latLngArray);
                    this.setPolygon(latLngArray, bin)
                    break;
                }
            case 'point':
                {
                    console.log('Bin typpe is point : ', bin)
                    console.log('Default : ', bin.loc.coordinates)
                    const latLng = { lat: bin.loc.coordinates[0], lng: bin.loc.coordinates[1] }
                    this.setCircle(latLng, bin)
                    break;
                }
            default:
                {
                    console.log('Default : ', bin.loc.coordinates)
                    // const latLng={lat:bin.loc.coordinates[0].lat,lng:bin.loc.coordinates[0].lng}
                    // this.setCircle(latLng)
                    break;
                }
        }

    }

    setPolygon(latLngArray, bin) {

        const color = localStorage.getItem('color') != undefined ? localStorage.getItem('color') : this.getRandomeColor();
        console.log('color :', color)
        var polyGonTriangle = new google.maps.Polygon({
            paths: latLngArray,
            strokeColor: color, //'#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color, //this.getRandomeColor(),
            fillOpacity: 0.35
        });
        this.shapes.push(polyGonTriangle);
        polyGonTriangle.setMap(this.map);
        // }, 1000)
        var infoWindow = new google.maps.InfoWindow;
        console.log("polyGonTriangle", polyGonTriangle, bin.geo_nm)

        polyGonTriangle.addListener('click', function(event) {
            // Since this polygon has only one path, we can call getPath() to return the
            // MVCArray of LatLngs.
            var self: any = this;
            var vertices = self.getPath();
            var infoWindow = new google.maps.InfoWindow;
            var contentString = "<b>" + bin.geo_nm + "</b>";
            console.log("at showArrays", event);


            // Replace the info window's content and position.
            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);

            infoWindow.open(this.map);
        });
    }

    setCircle(latLng, bin) {
        console.log('latLng : ', latLng);
        const color = localStorage.getItem('color') != undefined ? localStorage.getItem('color') : this.getRandomeColor();
        var circle = new google.maps.Circle({
            strokeColor: color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.35,
            map: this.map,
            center: latLng,
            radius: 120
        });
        this.shapes.push(circle);
        var infoWindow = new google.maps.InfoWindow;
        console.log("polyGoncircle", circle)

        circle.addListener('click', function(event) {
            // Since this polygon has only one path, we can call getPath() to return the
            // MVCArray of LatLngs.
            var self: any = this;
            // var vertices = self.getPath();
            var infoWindow = new google.maps.InfoWindow;
            var contentString = "<b>" + bin.geo_nm + "</b>";
            console.log("at showArrays", event);


            // Replace the info window's content and position.
            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);

            infoWindow.open(this.map);
        });
    }

    getRandomeColor() {
        var letters = '012345'.split('');
        var color = '#';
        color += letters[Math.round(Math.random() * 5)];
        letters = '0123456789ABCDEF'.split('');
        for (var i = 0; i < 5; i++) {
            color += letters[Math.round(Math.random() * 15)];
        }
        return color;
    }
    setLatLongInObj(array) {
        // console.log('setLatLongInObj array : ',array)
        let newArray = []
        array.forEach((coords, i) => {
            newArray.push({ lat: coords[0], lng: coords[1] })
            //   console.log('triangleCoords : ',array[i])
        })
        return newArray;
    }

    getDeviceLocationByInterval(imei) {
        let obj: any = {
            imei: imei.imei
        }
        this.interval = setInterval(() => {
            // this.trackingData = "";
            this.clearMarkers();
            this.clearInfoWindow();
            this.mapService.getDeviceLocation(obj).subscribe(
                (data: any) => {
                    // this.spinnerService.hide();
                    console.log("tracking data", data.data);
                    this.trackingData = data.data;
                    _.remove(this.trackingData, function(data) {
                        return data.result == null;
                    });
                    this.trackingData.forEach((element) => {
                        element.result.ignitionStatus = element.result.ignition == true ? 'On' : 'Off';
                    })
                    console.log("filert this.trackingData", this.trackingData);

                    this.placeMarker();
                }
            );
        }, 50000)
    }

    getLiveLocation(lat, long, flag) {
        if (flag == true) {
            this.map = new GMaps({
                el: "#map",
                lat: lat,
                lng: long
            });
            this.map.addMarker({
                lat: lat,
                lng: long
            });
        } else {
            this.map = new GMaps({
                el: "#map",
                lat: lat,
                lng: long
            });
        }

    }
    getCurrentPosition() {
        let currentLocation: any = {};
        navigator.geolocation.getCurrentPosition(position => {
            console.log("position : ", position);
            this.lat = position.coords.latitude;
            this.long = position.coords.longitude;
            currentLocation = { lat: this.lat, long: this.long }
            localStorage.setItem('currentLocation', JSON.stringify(currentLocation))
        });
        if (localStorage.getItem('currentLocation') != undefined) {
            this.currentLocation = JSON.parse(localStorage.getItem('currentLocation'));
            this.lat = currentLocation.lat;
            this.long = currentLocation.long;
        }
    }

    reverseGeoCoding() {
        fetch(
            "https://maps.googleapis.com/maps/api/geocode/json?address=" +
            this.lat +
            "," +
            this.long +
            "&key=" +
            "AIzaSyAD3BujxZbC4o8V7WijuOxZ8x5E4zLpMQM"
        )
            .then(response => response.json())
            .then(responseData => {

                if (responseData.status === "OK") {
                    let geoCodingData = responseData.results;
                    console.log("geoCodingData", geoCodingData);

                    for (let i = 0; i < geoCodingData.length; i++) {
                        let geoBigLat = geoCodingData[i].geometry.location.lat;
                        let geoBigLong = geoCodingData[i].geometry.location.lng;
                        for (let j = 6; j >= 1; j--) {
                            let geoLat = geoBigLat.toFixed(j);
                            let geoLong = geoBigLong.toFixed(j);
                            console.log(geoLat, geoLong, this.lat, this.long, "same");
                            if (
                                geoLat == this.lat.toFixed(j) &&
                                geoLong == this.long.toFixed(j)
                            ) {
                                this.liveLocation = geoCodingData[i].formatted_address;
                                console.log("liveLocation", this.liveLocation);
                                break;
                            } else {
                                console.log(geoLat, geoLong, this.lat, this.long, "not same");
                            }
                        }
                        break;
                    }
                } else {
                    console.log("Please check your network connection and try again");
                }
            });
    }
}